################################################################################
In search of the perfect pin necklace
################################################################################

:date: 2024-11-16
:tags: pin


Last year, on the `<{article}pins>`__, I mentioned that I usually wear my pins on
a beanie, but was still looking for a good alternative for the warmer months.

After thinking more about it, I realized a necklace would be the way to go.
It doesn't depend on any specific piece of clothing and it's pretty visible, not
only to others but also to myself.

I started researching for pin necklaces and found a few different solutions for
the pin-to-necklace converter:

* `DIY Guide: Rubber pin back converter <https://www.epbot.com/2020/04/quick-craft-turn-any-disney-pin-into.html>`__
* `Buy: Transparent plastic converter <https://www.etsy.com/listing/1672516454/clearrings-necklace-converter-enamel-pin>`__
* `Buy: Leather patch converter <https://www.etsy.com/listing/607824060/enamel-pin-necklace-converter-wear-your?show_sold_out_detail=1&ref=nla_listing_details>`__
* `Buy: Locking pin back converter <https://cosmicmedium.com/products/lapel-pin-necklace-lock-converter?variant=40404834123973>`__

The guide is great, but I don't feel rubber pin backs are safe enough. And
neither the plastic nor the leather converters looked good in my opinion. But
the locking pin back one seemed perfect! So I ordered one right away.

Actually, I ordered two. The reason is that some pins have two posts,
side-by-side, so two pin backs would be needed to keep those pins correctly
oriented on the necklace (see pictures at the end). By buying two necklaces I
could transfer the converter from one of them over and have a necklace with two
converters, allowing me to wear any pin in my collection.

Once the necklace arrived and I started using it, I quickly realized there was a
big problem with it though: the pin kept getting turned around! That totally
beats the purpose of the necklace. I concluded the problem was the chain, which
needed to be switched for something stiff, like a leather cord. I settled on
this braided leather necklace (the 20inch in length and 3mm in diameter
variant):
https://www.amazon.com/Flexible-Braided-Leather-Necklace-Pendants/dp/B095W7J7D7

Once that arrived, I took two pliers, two jump rings (since the original ones
wouldn't close around this broader necklace) and used them to put the two pin
back converters on the new necklace. And it worked perfectly, no more pins
getting turned around! I also liked the way it looked much better, the chain was
too shiny and thin for my taste.

On top of that, since the jump rings I used are much broader than the necklace,
it's super easy to get the pin back converters in and out of it. That way, when
I'm wearing a pin that only uses one back, I take the other out so it's not
dangling and making noise.

Finally, here are the pictures of the end result:

One-post pin:

.. image:: {image}/df-front.jpg

.. image:: {image}/df-back.jpg

Two-posts pin:

.. image:: {image}/lotr-front.jpg

.. image:: {image}/lotr-back.jpg

Pins being worn:

.. image:: {image}/df-wear.jpg

.. image:: {image}/lotr-wear.jpg

The only issue is that the one-post pins, like the Dwarf Fortress one above,
always rest tilted to one side. I don't think there's a way to avoid that on a
necklace though. In any case, that's a minor issue and I am incredibly happy
with the result. I've been wearing pins everyday since!
