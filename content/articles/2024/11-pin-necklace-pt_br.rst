################################################################################
Em busca do colar de pins perfeito
################################################################################

:date: 2024-11-16
:tags: pin


Ano passado, no `<{article}pins>`__, eu mencionei que eu geralmente uso meus
pins na touca, mas que ainda estava procurando uma boa alternativa para dias
quentes.

Depois de pensar mais um pouco, eu concluí que um colar seria uma boa opção. Ele
não depende de nenhuma peça de roupa específica e é bem visível, tanto para os
outros quanto para mim.

Eu comecei a pesquisar por colares de pin e encontrei algumas soluções para o
conversor de pin para colar:

* `Guia DIY: Conversor usando tarraxa de borracha <https://www.epbot.com/2020/04/quick-craft-turn-any-disney-pin-into.html>`__
* `Comprar: Conversor usando plástico transparente <https://www.etsy.com/listing/1672516454/clearrings-necklace-converter-enamel-pin>`__
* `Comprar: Conversor usando um pedaço de couro <https://www.etsy.com/listing/607824060/enamel-pin-necklace-converter-wear-your?show_sold_out_detail=1&ref=nla_listing_details>`__
* `Comprar: Conversor usando tarraxa com trava <https://cosmicmedium.com/products/lapel-pin-necklace-lock-converter?variant=40404834123973>`__

O guia é ótimo, mas eu não acho que tarraxas de borracha são seguras o
suficiente. E eu não achei o conversor de plástico nem o de couro bonitos. Mas o
de tarraxa com trava pareceu perfeito! Então eu comprei um na hora.

Na verdade, eu comprei dois. O motivo é que alguns pins tem dois pinos, um do
lado do outro, então duas tarraxas seriam necessárias para deixar esses pins
orientados corretamente no colar (veja as fotos no final). Comprando dois
colares eu poderia transferir o conversor de um deles para o outro e ter um
colar com dois conversores, o que me permitiria usar qualquer pin da minha
coleção.

Porém assim que o colar chegou e eu comecei a usá-lo, ficou claro que ele tinha
um grande problema: o pin ficava virando o tempo todo! Isso acaba com o
propósito do colar. Eu concluí que o problema era a corrente, que tinha que ser
trocada por algo mais rígido, como um cordão de couro. Eu escolhi este colar de
couro trançado (a variante de 20 polegadas de comprimento e 3 milímetros de
diâmetro):
https://www.amazon.com/Flexible-Braided-Leather-Necklace-Pendants/dp/B095W7J7D7

Assim que ele chegou, eu peguei dois alicates, dois anéis de metal próprios para
bijuteria (já que os originais não iriam fechar nesse colar mais grosso) e usei
eles para colocar os dois conversores de pin no novo colar. E funcionou
perfeitamente, os pins não viraram mais! Eu também gostei mais da nova
aparência, a corrente era muito brilhante e fina para o meu gosto.

Além disso, como os anéis que eu usei são bem mais largos que o colar, é muito
fácil de tirar e pôr os conversores de pin. Graças a isso, quando eu estou
usando um pin que só usa um conversor, eu tiro o outro para não deixar ele
pendurado fazendo barulho.

Finalmente, aqui estão as fotos do resultado final:

Pin de um pino:

.. image:: {image}/df-front.jpg

.. image:: {image}/df-back.jpg

Pin de dois pinos:

.. image:: {image}/lotr-front.jpg

.. image:: {image}/lotr-back.jpg

Colar no corpo:

.. image:: {image}/df-wear.jpg

.. image:: {image}/lotr-wear.jpg

O único problema é que os pins de um pino, como o do Dwarf Fortress acima,
sempre ficam virados para o lado. Mas eu acho que não tem como evitar isso em um
colar. De qualquer forma, é só um detalhe e eu estou extremamente feliz com o
resultado. Eu tenho usado pins todo dia desde então!
