#############################################
vCard + RSS como alternativa às redes sociais
#############################################

:date: 2024-03-22
:tags: vcard, rss, atom, redes sociais


Ano passado depois de conversar um pouco com alguém durante uma conferência, ele
perguntou o meu LinkedIn para manter contato, e eu respondi que não tenho.

Já fazem vários anos desde que eu decidi sair das redes sociais. Eu não sinto
falta. Aplicativos de mensagem instantânea me permitem manter contato com as
pessoas que realmente são importantes para mim em um nível muito mais pessoal.

Ainda assim, essa interação ficou na minha cabeça. De fato seria legal ter um
registro dessas conexões também. As pessoas com quem você teve uma conversa
interessante por alguns minutos. Poder checar como elas estão de vez em quando,
onde estão trabalhando e no que estão hackeando.

Basicamente eu gostaria que todo mundo tivesse um perfil online e um feed, que
eu pudesse facilmente checar e seguir, e que os dados associados a eu estar
seguindo pudessem ser armazenados localmente. Por que isso precisa ser exclusivo
a redes sociais?

Não é. Na verdade, a parte do feed já foi resolvido há muito tempo com o
RSS_/Atom_. Então só é necessário ter a URL do feed RSS/Atom de alguém, que você
pode adicioná-la ao seu leitor de feed e facilmente seguir as atualizações de
uma pessoa independente de onde elas são publicadas.

.. _RSS: https://en.wikipedia.org/wiki/RSS
.. _Atom: https://en.wikipedia.org/wiki/Atom_(web_standard)

A parte do perfil, um lugar com pontos chave sobre o estado atual de uma pessoa
(nome, foto, localizaçäo, empresa, etc) é a que historicamente foi mais atrelada
a redes sociais. Sites pessoais muitas vezes tem uma página "sobre" com essas
informações, mas por conta da falta de estrutura eu não acho elas tão úteis
quanto os perfis de redes sociais.

Mas na verdade existe um padrão aberto amplamente usado para armazenar perfis
também: vCard_. Ele é conhecido princpalmente como o formato usado por clientes
de email para armazenar contatos, os quais são até sincronizados entre servidor
e clientes usando o protocolo CardDAV_. Mas vCards também são usados para
contatos no celular: é possível importar e exportar contatos no iOS e Android
usando vCards.

.. _vCard: https://en.wikipedia.org/wiki/VCard
.. _cardDAV: https://en.wikipedia.org/wiki/CardDAV

Então foi aí que eu percebi, se existe um padrão aberto amplamente usado para
feeds, e outro para perfis, por que não juntar os dois? Ao colocar a URL de um
feed RSS/Atom em um vCard, ele se torna um único arquivo com toda a informação
necessária para identificar uma pessoa e acompanhar as atualizações dela. Por
ser um único arquivo, é fácil de compartilhar com os outros e também de
armazenar os vCards de todas as suas conexões localmente.

Agora, um ponto interessante é que apesar da informação de perfil em um
vCard poder ficar desatualizada, uma das propriedades disponíveis é a ``SOURCE``
que tem como objetivo armazenar a URL de onde a última versão do vCard pode ser
obtida. Ou seja, ao hospedar seu vCard em uma URL pública, por exemplo no seu
site pessoal, é possível que pessoas que já baixaram ele o mantenham atualizado.

Quanto a como a URL do feed pode ser incluída no vCard, depois de ler por cima a
`última versão da especificação`__, eu originalmente tinha a intenção de que ela
fosse uma propriedade ``URL`` padrão. Mas eu queria que a URL principal do vCard
ainda fosse a do site da pessoa, então algum mecanismo seria necessário para
diferenciar entre as duas. Minha primeira ideia foi usar o parâmetro
``MEDIATYPE`` na ``URL``, mas a especificação diz explicitamente que para
protocolos como o HTTP o cabeçalho HTML ``Content-type`` deveria ser usado para
isso. Porém, aparentemente, apesar de feeds RSS/Atom terem *media type* próprios
padronizados (``application/rss+xml`` e ``application/atom+xml``
respectivamente), na prática, ``text/xml`` é usado, o que não os identifica como
feed. Então no fim, para conseguir determinar que uma URL em um vCard é de um
feed, a URL teria que ser baixada e o arquivo lido. Para evitar essa
complicação, eu decidi, relutantemente, usar uma propriedade não padronizada,
``X-FEED``, para o feed.

.. __: https://datatracker.ietf.org/doc/html/rfc6350

Testando na prática
===================

Para conseguir testar a ideia na prática, eu criei `esse repositório`__ com dois
scripts simples. O primeiro é o script ``vcard-render`` que lê vCards e
renderiza eles usando um template jinja_. A ideia é que você pode usá-lo para
renderizar o seu vCard e o vCard de suas conexões no seu site, que é exatamente
como eu estou usando ele `nesta página que eu adicionei ao meu site`__ (ela pode
ser encontrada através da página Sobre). Para detalhes sobre como eu integrei o
script no meu website, veja `este commit`__.

.. _jinja: https://en.wikipedia.org/wiki/Jinja_(template_engine)

.. __: https://codeberg.org/nfraprado/vcard-network
.. __: https://nfraprado.net/br/pages/vcard-network.html
.. __: https://codeberg.org/nfraprado/blog/commit/148c9cf31eb10434ba127e94332e51397f18db99

O outro script é o ``vcard-to-opml`` que extrai feeds de vCards e gera um
arquivo OPML com eles, o qual pode então ser importado em um leitor de feeds. A
ideia é usar esse script nos vCards de todas as pessoas que você baixou (suas
conexões) para que você possa facilmente seguir os feeds delas. Eu uso o leitor
de feeds newsboat_, então eu adicionaria o seguinte comando em um crontab para
rodá-lo periodicamente (assim que eu tiver algumas conexões de verdade!):

.. code::

   newsboat -i <(awk 1 ~/ark/etc/dav/contacts/5DEB-65D21680-2F1-4AEA3480/* | ./vcard-to-opml.py)

.. _newsboat: https://newsboat.org/

Nota: ``awk`` é usado aqui ao invés de um simples ``cat`` porque aparentemente
esses vCards não têm ``\n`` no final do arquivo. Não sei se isso é culpa do meu
provedor de email, onde eu guardo meus vCards, ou do vdirsyncer_, que é o
programa que eu uso para manter uma cópia local dos meus vCards.

.. _vdirsyncer: http://vdirsyncer.pimutils.org/en/stable/

Conclusão
=========

Se você tiver interesse em experimentar usar vCards e RSS/Atom para conectar com
pessoas também, dê uma olhada no repositório__, que contém esses scripts. E
fique à vontade para entrar em contato para me dizer que está usando, você pode
achar meu email no... claro, no meu vCard 😉.

.. __: https://codeberg.org/nfraprado/vcard-network

Mesmo se ninguém usar isso, pelo menos agora eu tenho uma boa resposta para a
próxima pessoa que pedir meu LinkedIn. "Escaneie esse QR code e você vai ver meu
perfil" 🙂.

.. image:: {image}/qrcode_br.png
