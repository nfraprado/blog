#############################################
vCard + RSS as an alternative to social media
#############################################

:date: 2024-03-22
:tags: vcard, rss, atom, social media


Last year after talking for a while with someone during a conference they asked
me for my LinkedIn to be able to connect with me, to which I answered I didn't
have one.

It was many years ago when I decided to leave social media. I don't miss it.
Instant messaging allows me to keep in touch with the people that really matter
to me in a much more personal way.

Still, this interaction stayed in the back of my mind. It would indeed be nice
to keep track of these connections too. The people you had an interesting
conversation with for just a few minutes. To be able to check how they're doing
every now and then, who they're working for and what they're hacking on.

Essentially I would like for everyone to have an online profile and feed, which
I could easily check and subscribe to, and that this subscription data could be
stored locally. Why should that be exclusive to social media?

It's not. In fact, the feed part has already been solved for quite a long time
with RSS_/Atom_. So by just having the URL for someone's RSS/Atom feed, you can
add it to your feed reader and easily follow the person's updates wherever
they're published to.

.. _RSS: https://en.wikipedia.org/wiki/RSS
.. _Atom: https://en.wikipedia.org/wiki/Atom_(web_standard)

The profile part, a place with the key points about a person's current status
(name, photo, location, employer, etc), is the one that has historically been
tied to social media. Personal websites do many times have an "about" page with
such information, but I find the lack of structure makes it way less useful than
a social media's profile.

However, there's actually a widely used open standard for encoding profiles as
well: vCard_. It's mainly known as the format used on email clients to store
contacts, where they're even synchronized between server and clients using the
CardDAV_ protocol. But vCards are also used for contacts in your phone: it's
possible to import and export contacts on iOS and Android using vCards.

.. _vCard: https://en.wikipedia.org/wiki/VCard
.. _cardDAV: https://en.wikipedia.org/wiki/CardDAV

So that's when I realized, if there's a widely used open standard for feeds, and
another for profiles, why not join the two? By embedding an RSS/Atom feed URL in
a vCard, it becomes a single file that has all the information needed to
identify a person and follow their updates. Since it's a single file, it's easy
to share it with others as well as store the vCards for all your connections
locally.

Now one interesting bit is that though the profile information on the vCard
might get outdated, there is actually a ``SOURCE`` property which is meant to
contain the URL where the latest version of the vCard can be retrieved. So by
hosting your vCard in a public URL, say your personal website, it's possible for
people that have already downloaded it to keep it up to date.

As for how the feed URL can be encoded in the vCard, after skimming over
`the latest version of the spec`__, I originally intended to have it as a plain
``URL`` property. But I wanted the main URL in the vCard to still be the
person's website, so some mechanism would be needed to differentiate between the
two URLs. At first I thought of using a ``MEDIATYPE`` parameter in the ``URL``,
but the spec explicitly said that for protocols such as HTTP the
``Content-type`` HTML header should be used instead. However, as it seems,
despite RSS/Atom feeds having standardized media types (``application/rss+xml``
and ``application/atom+xml`` respectively), in practice, ``text/xml`` is used
instead. In the end, this means that in order to tell that an URL in the vCard
is for an RSS/Atom feed, the URL would need to be fetched and read. To avoid
that complication, I decided, reluctantly, to use a non-standard property
instead, ``X-FEED``, for the feed.

.. __: https://datatracker.ietf.org/doc/html/rfc6350

Trying it out
=============

In order to try the idea out, I created `this repository`__ with a couple simple
scripts. First there's a ``vcard-render`` script that parses vCards and renders
them using a provided jinja_ template. The idea is that you can use it to render
your vCard and your connections' vCards in your website for example, which is
exactly what I use it for `in this page I've added to the website`__ (it can be
found from the About page). If you're curious as to how I integrated the script
into my website, see `this commit`__.

.. _jinja: https://en.wikipedia.org/wiki/Jinja_(template_engine)

.. __: https://codeberg.org/nfraprado/vcard-network
.. __: https://nfraprado.net/pages/vcard-network.html
.. __: https://codeberg.org/nfraprado/blog/commit/148c9cf31eb10434ba127e94332e51397f18db99

The other script is ``vcard-to-opml`` which extracts the RSS feed from the
vCards supplied and generates an OPML file with them, which can then be imported
into a feed reader. The idea here is to run this script on the vCards of all the
people you have downloaded (your connections) so you can easily follow their
feeds. I use newsboat_ as my feed reader, so I would add the following command
to a crontab to run it periodically (as soon as I get some real connections!):

.. code::

   newsboat -i <(awk 1 ~/ark/etc/dav/contacts/5DEB-65D21680-2F1-4AEA3480/* | ./vcard-to-opml.py)

.. _newsboat: https://newsboat.org/

Note: ``awk`` is used here instead of a simple ``cat`` because apparently those
vCards don't have a newline at the end of the file. I'm not sure if it's my
email provider's fault, where I store my vCards, or vdirsyncer_'s fault, which
is the program I use to have a local copy of my vCards.

.. _vdirsyncer: http://vdirsyncer.pimutils.org/en/stable/

Conclusion
==========

If you're interested in trying out using vCards and RSS to connect to people as
well, check out the repository__ that contains those scripts. And feel free to
reach out if you do so, you can find my email on... you guessed it, my vCard 😉.

.. __: https://codeberg.org/nfraprado/vcard-network

Even if no one ever uses this, at least I now have a proper answer for the next
person that asks for my LinkedIn. "Just scan this QR code and you'll see my
profile" 🙂.

.. image:: {image}/qrcode.png
