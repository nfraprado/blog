##############
Um ano de blog
##############

:date: 2021-06-22
:tags: aniversário-blog, blog
:series: Um ano de blog


Faz um ano que eu comecei esse blog! Pensei em aproveitar a oportunidade para
falar um pouco sobre o blog em si: Como começou e meus pensamentos sobre ele.

Origem
======

Eu já tinha pensado em pouco sobre ter meu próprio blog. Ter um cantinho da
internet onde eu fosse livre para falar sobre coisas que me interessam parecia
bem legal. E quem sabe outras pessoas também achariam elas interessantes!

Mas eu não tenho interesse nenhum em desenvolvimento web, então eu só queria
escrever meus artigos sem precisar lidar com isso. Isso parecia apontar para
geradores de site prontos para uso como o WordPress, mas eu também não gostava
dessa ideia.

Felizmente, já existia uma certa tendência a usar geradores de site estático.
Eles eram rápidos, simples de usar e você podia versionar o fonte no git e
hospedar o site de graça usando Gitlab/Github! Assim que eu soube da existência
deles eu sabia que era isso que eu queria.

Na época dois dos meus amigos já tinham seus próprios blogs
(https://rafaelg.net.br e https://andrealmeid.com) usando geradores de site
estático e eles me encorajaram a criar o meu também. Um deles me introduziu ao
pelican_, um gerador de blog estático em python e eu decidi experimentar ele.

.. _pelican: https://github.com/getpelican/pelican/

Depois de escolher um tema e um pouco de customização no pelican (que eu vou
falar sobre no próximo artigo), eu estava pronto para começar a escrever. E foi
então que eu comecei o blog com o `<{article}tasklist>`__, que é divertidamente
pessoal, e ainda sim técnico. Eu gosto desse estilo 🙂.

Então foi assim que o blog começou. Agora eu queria ir um pouco mais a fundo nos
princípios que guiam o blog.

Princípios
==========

Existem duas coisas que eu acho que são bem importantes sobre o jeito que o blog
funciona: o idioma duplo e o formato de diário aberto.

Idioma duplo
------------

Você deve ter notado que todos os artigos no meu blog têm um campo de
"traduções" com um outro idioma. E também há um botão para mudar o idioma na
barra de navegação. O motivo é que o meu blog está disponível tanto em inglês
quanto em português brasileiro. Para tornar isso possível eu traduzi a interface
para português e, mais importante, escrevo cada artigo tanto em inglês quanto em
português.

A interface foi bem fácil de traduzir, mas precisar escrever cada artigo
praticamente duas vezes é bastante trabalho a mais. Então por que eu faço isso?

Quando eu estava pensando em criar o blog, eu me deparei com um dilema: em qual
idioma deveria escrever?

Por um lado, escrever em inglês faria meu conteúdo amplamente disponível, já que
é, na prática, a língua da internet. Seria possível por exemplo compartilhar um
artigo com uma comunidade com a qual eu contribuí.

Por outro lado, eu gosto da minha língua. E eu queria poder compartilhar meus
artigos com pessoas que eu conheço pessoalmente sem um barreira invisível:
quando tanto eu quanto a pessoa somos mais familiares com português, parece sem
sentido transmitir a informação em inglês. Eu também não queria alienar pessoas,
do meu próprio país, que não falam inglês (apesar disso ser cada vez mais raro).

Então eu decidi escolher os dois 🙂. O dobro do trabalho, mas nenhuma das
desvantagens e o dobro das vantagens. Na verdade não é o dobro do trabalho, já
que o segundo artigo é basicamente a tradução do outro e não um artigo do zero.
O fato de eu não publicar tão frequentemente (uma vez por mês) também ajuda.

Formato de diário aberto
------------------------

Outra coisa é a forma que eu lido com meus artigos, e minha motivação. Ou a
resposta à pergunta: Já existe um zilhão de blogs por aí, por que alguém iria
querer ler o meu? Realmente vale a pena ter um blog?

E minha resposta para isso é: Sim, vale, porque eu não estou escrevendo para os
outros.

A principal razão de eu escrever no meu blog é porque eu quero registrar os
projetos que eu fiz e as coisas que eu achei interessantes, para que no futuro
eu possa olhar para trás e relembrar deles. Então é basicamente um diário
(só que mensal).

Mas claro que se qualquer pessoa estiver interessada em qualquer um dos
assuntos, ela é livre para ler, o que torna o blog mais como um diário aberto.
Bem legal se acontecer, mas eu não conto com isso. Eu escrevo para mim mesmo,
mas se outras pessoas gostarem é um ótimo bônus 🙂.

Esse é o motivo de eu não costumar fazer tutoriais. Ao invés disso documento
minha experiência no aprendizado ou construção de algo. Tem um quê bem mais
pessoal, apesar de não ser tão acessível.

(Agora que eu escrevi esses dois princípios eu percebi que eles são um pouco
contraditórios, mas fazer o que, é como eu me sinto. Eu sou humano afinal de
contas 😛.)

Já que eu estava tratando meu blog como um diário fazia sentido para mim tentar
escrever com uma frequência fixa. Eu acabei decidindo escrever uma vez por mês
já que era o suficiente para me manter engajado mas não demais a ponto de me
estressar.

Eu também acabei estabelecendo o dia 20 do mês como a data alvo. Isso é só uma
diretriz. Eu não me forço nada disso. Se eu não estiver disposto a publicar
nesse mês ou se não conseguir a tempo do dia 20, tudo bem. Mas estabelecer esse
prazo opcional cria uma pressão saudável para que eu vença a preguiça e pense se
alguma coisa interessante que eu queira compartilhar (com meu eu do futuro e com
outras pessoas) aconteceu recentemente.

Isso tem funcionado muito bem até agora. Eu sinto que eu sempre tive algo
interessante para compartilhar, e de fato eu publiquei todos os meses até o
momento, só me atrasei algumas vezes com relação ao dia 20 (inclusive para este
artigo 😝). Me limitando a um único artigo por mês eu também acabo criando um
acúmulo de artigos para publicar, apesar de geralmente faltar alguma coisa neles
antes de poderem ser publicados (eu tenho pelo menos três nessa situação neste
momento).

Conclusão
=========

Então, em resumo, tem sido uma jornada bem legal até agora. Eu estou feliz de já
ter acumulado 13 artigos (14 contando este) que eu posso rever no futuro e ficar
nostálgico pelas coisas que eu construí ou aprendi, e quem sabe até ter novas
ideias para projetos futuros!

Eu não sei até quando isso aqui vai continuar, mas por enquanto eu não vejo um
fim 🙂.

E é isso sobre o lado "humano" do blog. No próximo artigo eu vou falar sobre o
lado técnico: as customizações que eu fiz no blog ao longo desse ano para suprir
minhas necessidades.
