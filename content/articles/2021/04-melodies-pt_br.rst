#############################################
Aprendendo teoria musical escrevendo melodias
#############################################

:date: 2021-04-20
:tags: música, teoria-musical, piano


Olha só, primeiro artigo não-técnico no blog 🙂.

Enfim, eu não cheguei a falar sobre isso por aqui antes, mas eu gosto bastante
de tocar piano. Quando eu era mais novo, eu fiz aulas de violão, mas eu sempre
quis tocar piano, e alguns anos atrás eu finalmente comprei um! Eu tenho tocado
ele desde então, apesar nunca ter realmente feito aulas e nem praticado
intensamente, mas eu me divirto bastante com ele.

O que eu mais gosto de fazer é improvisar. Eu acho bem relaxante, e às vezes
surgem coisas interessantes que eu quero desenvolver melhor e transformar em uma
música.

Outra coisa que eu tenho bastante interesse é teoria musical. Eu acho fascinante
como é possível dissecar as músicas e chegar nos blocos básicos que as formam e
descobrir o que as fazem soar bem. Além disso, eu acho que se eu aprender mais
teoria musical eu vou me divertir mais improvisando e compondo 😀.

Duas ótimas referências que eu estou usando atualmente para aprender teoria
musical são:

* `Dave Conservatoire`__
* `Curso "Approaching Music Theory: Melodic Forms and Simple Harmony" no
  Coursera`__

.. __: https://www.daveconservatoire.org/
.. __: https://www.coursera.org/learn/melodic-forms-simple-harmony

O Dave Conservatoire é basicamente um Khan Academy para teoria musical e é muito
bom! Eu estou revendo os conceitos básicos e aprendendo algumas coisas novas
nele.

O curso do Coursera é o que eu estou mais animado, porque ele ensina os blocos
básicos da música de um jeito que é divertido, e analisando músicas existentes.
Além do mais, há tarefas em que você precisa compor sua própria pequena melodia
em estilos diferentes. E é sobre isso que eu quero falar nesse artigo!

Até o momento no curso, eu tive que escrever melodias em três estilos
diferentes: canto gregoriano, jazz e música tradicional. Eu tentei compor apenas
escrevendo no papel, mas sinceramente acabei dependendo bastante de tocar as
notas no piano para ouvir como cada uma soava.

Depois que eu acabei cada melodia, eu a transcrevi para uma partitura mais bem
feita usando o MuseScore_, que é um programa de código aberto sensacional para
escrever partituras. O que é ainda mais incrível é que ele é capaz de tocar suas
partituras usando MIDI! Então para cada melodia eu exportei tanto a partitura
quando o áudio para mostrar aqui.

.. _MuseScore: https://musescore.org

Minhas melodias
===============

Canto gregoriano
----------------

A ideia no canto gregoriano era fazer algo usando um conjunto bem limitado de
notas e poucos saltos, mas como você pode ver eu quebrei essa segunda regra,
apesar que eu acho que soou bem.

Por ser um `canto gregoriano`_, a partitura deveria ter apenas 4 linhas e não
ter compassos, deveria ter um andamento fluido. Mas o MuseScore não foi feito
para isso, então é por isso que tanto o áudio quanto a partitura são mais
"robóticos" do que eu gostaria.

.. _canto gregoriano: https://pt.wikipedia.org/wiki/Canto_gregoriano

`Ouça aqui <{static}/audio/melodies/gregorian_chant.mp3>`__

.. image:: {image}/gregorian_chant.png
   :alt: Partitura da melodia de canto gregoriano

Jazz lento
----------

Já para o jazz lento deveria haver uma noção de tempo mais estrita, mas ter os
tons mais livres, sendo inclusive encorajada a presença de notas alteradas.

Para as alterações eu usei um Lá e um Ré naturais, mas como você pode ver, não
há nenhum Lá sustenido para fazer com que os naturais soem fora da armadura de
clave, o que eu só aprendi depois. Isso é definitivamente algo que eu gostaria
de melhorar da próxima vez.

Eu também acho que eu sem querer coloquei muito uma estética da trilha sonora do
Genshin Impact, que apesar de ser bonita, não é jazz 😛.

`Ouça aqui <{static}/audio/melodies/slow_jazz.mp3>`__

.. image:: {image}/slow_jazz.png
   :alt: Partitura da melodia de jazz lento

Música tradicional
------------------

Por fim, a melodia de música tradicional deveria ser algo mais repetitivo e
fácil de lembrar e cantar junto. Eu fiz em pentatônica porque aprendi que
funcionava bem, e eu também acho que ter um ritmo interessante é importante,
então foi o que eu fiz.

`Ouça aqui <{static}/audio/melodies/folk.mp3>`__

.. image:: {image}/folk.png
   :alt: Partitura da melodia de música tradicional

Isso é tudo por enquanto, mas espero compartilhar mais em breve 🙂.
