##########################################
Minha jornada até um bom sistema de backup
##########################################

:date: 2021-09-27
:tags: backup, borg


Eu confesso que sou um pouco acumulador de dados. Eu ainda tenho alguns dos
primeiros programas que escrevi, fotos que tirei durante viagens e desenhos que
fiz muitos anos atrás, para citar alguns exemplos. E já que eu não confio em
alguma empresa para armazenar todos esses dados para mim, já era bem claro que
eu precisava fazer meus próprios backups.

Uma lição que eu aprendi cedo lendo online é que backups devem ser o mais fácil
possível de fazer, preferencialmente totalmente automáticos, se não você acaba
nem fazendo eles.

Nesse artigo eu quero relembrar meu sistema de backup anterior e então falar
sobre o atual. Provavelmente até o final vai ficar claro que o atual é muito
mais simples e melhor.

Antigamente
===========

Eu acho que foi em 2018 que eu comecei a pensar seriamente sobre fazer backups.
Para isso eu precisava tanto de hardware e software. O hardware para conter os
backups e o software para fazê-los acontecer.

Hardware
--------

Eu tinha um computador de desktop que não funcionava mais devido a problemas na
placa-mãe, mas todo o restante funcionava bem, então era o candidato perfeito
para usar para o meu sistema de backup. O disco rígido poderia ser usado para
armazenar os backups, a fonte para fornecer energia a tudo, e o gabinete para
abrigar todos os componentes.

Eu comecei removendo a placa-mãe que não funcionava mais e percebi que a fonte
não fornecia nenhuma tensão por padrão. Pesquisando online pelo seu datasheet eu
descobri quais fios precisavam ser curto-circuitados para que ela ligasse. Já
que o botão de ligar no gabinete era um push button que conectava na placa-mãe,
que por sua vez curto-circuitava esses fios, eu também removi esse botão e
coloquei uma chave liga-desliga no lugar. Eu soldei a chave nos fios da fonte e
grudei ela no gabinete com fita isolante e umas plaquinhas finas de metal que eu
tinha sobrando para dar um pouco de rigidez. Ficou assim:

.. image:: {image}/button.jpg

Quanto ao componente principal, o computador que faria os backups, eu tinha uma
Raspberry Pi que não estava sendo usada então ela era a escolha óbvia. Eu peguei
um conector micro USB de um cabo velho, abri o cabo, e soldei seus fios nos fios
de 5V e terra da fonte. Assim, eu conseguiria energizar tudo pela mesma fonte,
já que o disco rígido já era alimentado por ela.

Finalmente, já que o disco rígido era interno, e portanto usava conexão SATA
para transferir dados, eu comprei um cabo conversor SATA para USB baratinho e
usei ele para conectar o disco na raspberry. A montagem completa fica assim:

.. image:: {image}/internals.jpg

(Essa é uma foto atual, na época a raspberry usava WiFi ao invés do cabo
Ethernet)

E com isso o hardware estava completo, o próximo passo era a solução de software
para fazer os backups acontecerem.

Software
--------

Nessa época em que eu estava pensando sobre qual software usar para os meus
backups, eu esbarrei `nesse artigo`__. Ele me apresentou as ideias de backup
incremental (apenas armazenar as diferenças dos arquivos do backup anterior) e
rotação de backup (apenas armazenar os últimos N backups, deletando os mais
antigos) que eu achei geniais. Ele também se baseava no rsync para as cópias,
que era um programa com o qual eu já estava acostumado. No artigo um shell
script é escrito para fazer o backup e a retenção, mas eu vi em um comentário
mencionando que existia uma ferramente que fazia exatamente isso, chamada
rsnapshot_. Então eu decidi usar rsync + rsnapshot como minha solução de backup.

.. __: https://opensource.com/article/18/8/automate-backups-raspberry-pi
.. _rsnapshot: https://rsnapshot.org/

Em pouco tempo eu já comecei a me deparar com problemas. O maior deles era que o
rsnapshot só consegue fazer backups para um armazenamento local. Isso significa
que eu precisava rodar ele na raspberry, que estava conectada no disco rígido
onde os backups iriam ficar, e de lá fazer backup dos arquivos do meu
computador.

Já que a raspberry precisava conectar no meu notebook, o normal a se fazer seria
configurar um IP fixo nele, mas já que eu carregava ele comigo para outras
redes, eu pensei que seria melhor mantê-lo com IP dinâmico. Então eu acabei com
uma solução não-exatamente-elegante em que o meu notebook rodava o cronjob para
o backup, no qual ele conectava à raspberry, atualizava seu IP nela, e rodava o
rsnapshot nela. O rsnapshot então usava o IP atualizado para conectar de volta
ao notebook e fazer o backup.

Para tornar os dados seguros, o disco era criptografado usando dm-crypt.
Portanto antes do rsnapshot começar o backup, o disco era desbloqueado usando a
senha, que ficava salva como um arquivo no sistema da raspberry (eu sei, muito
inseguro!). Depois do backup ele era bloqueado de novo e desligado.

Outro problema era que o rsync não lida muito bem com renomeação ou
mudança de arquivos/diretórios. Ele pensa que o anterior foi deletado e um novo
foi criado. Isso era um grande problema já que eu estava sempre tentando achar a
melhor hierarquia para os meus arquivos, o que significava renomear ou mover os
diretórios base e de repente todos os meus arquivos precisavam ser copiados de
novo...

Esse problema era agravado ainda mais pelo fato de que a transferência entre o
meu computador e a raspberry era feita sem fio, então era bem lenta. Para
contornar esse problema eu adicionei um pouco de lógica nos scripts de backup
para que se o tamanho total da transferência fosse muito grande, ela
simplesmente falhava e me notificava. Então eu olhava no diff, lembrava que eu
tinha renomeado alguns diretórios e copiava eles para o backup manualmente
através de um cabo Ethernet temporário.

Por fim, como a fonte era bem barulhenta e eu também não queria deixá-la ligada
à toa, eu colocava um alarme diário no meu celular para me lembrar de apertar o
botãozinho para ligá-la logo antes da hora do backup diário. E depois eu
desligava antes de dormir.

Atualmente
==========

Um bom tempo passou desde então, e já que aquele sistema funcionava mas estava
longe de ser bom, eu pesquisei melhor sobre as ferramentas de backup que existem
para ver se achava algo melhor. Tudo mudou quando eu descobri o borg_.

.. _borg: https://www.borgbackup.org/

Borg_, ou BorgBackup, é um programa para fazer backups. Ele consegue fazer
backups locais ou remotos. Os backups podem (e devem!) ser encriptados. E
deduplicados! Eu não tenho certeza se renomear/mover arquivos fazem o backup
levar mais tempo, mas no mínimo ele não vai ocupar mais espaço já que todos os
arquivos são divididos em pequenos pedaços e deduplicados independentemente do
caminho. Ah, e ele tem uma ótima documentação e uma comunidade ativa!

Além disso, existe um outro projeto chamado borgmatic_. O borgmatic permite que
toda a configuração do uso do borg seja feita em um único arquivo, como os
repositórios para usar, a política de retenção e os arquivos para ignorar. Então
basicamente enquanto o borg fornece os comandos para fazer os backups, o
borgmatic permite que tudo seja configurado em um único arquivo e ele cuida do
resto.

.. _borgmatic: https://torsion.org/borgmatic/

No fim, borg + borgmatic fazem ser incrivelmente fácil de ter backups seguros e
confiáveis. Eu agora tenho uma única entrada simples no cron para fazer os
backups diários: ``0 21 * * * cd ~/ && borgmatic --no-color --verbosity 1
--files >> ~/ark/etc/bkplogs``. É importante notar que o borg sempre usa
caminhos relativos nos backups, então por isso que eu dou ``cd`` para a pasta
onde os arquivos que eu quero salvar estão (meu diretório home).

Além disso, como toda a minha configuração dos backups é feita em um único
arquivo de configuração com o borgmatic, é fácil de fazer backup desse arquivo
também. Então se algum dia eu perder meus arquivos e precisar recuperar do
backup, eu vou imediatamente já ter a configuração do backup feita e pronto para
continuar fazendo backups.

Mas é, o borg é realmente ótimo e muito melhor do que todas as outras soluções
que tentei antes. Tem um único detalhe, se você está fazendo backup para um
repositório remoto, o borg também tem que rodar lá. Então nesses casos você vai
ter um borg rodando na máquina local, fazendo a divisão dos arquivos em pedaços
e criptografando (para que o remoto já receba as partes criptografadas e seja
seguro!), e na máquina remota outra instância do borg vai estar recebendo eles e
armazenando-os no repositório.

Dada essa restrição, você não pode fazer backup para um armazenamento remoto
simples na nuvem. Mas existem alternativas muito boas focadas em hospedar
backups de borg, como o BorgBase_.

.. _BorgBase: https://www.borgbase.com/

Mas bem, eu ainda uso minha montagem da raspberry, só que agora eu instalei o
borg lá ao invés do rsnapshot, e removi todos os meus scripts estranhos. Ele é
agora apenas mais uma entrada no meu config do borgmatic. Fora isso, ao invés de
ter a senha do disco simplesmente jogada no sistema da raspberry eu tive a ideia
de enviá-la como parte do comando ssh da minha máquina, assim eu posso armazenar
a senha de forma segura na minha própria máquina que é criptografada. Ah, e
agora eu acabo fazendo os backups diários para um servidor remoto e apenas faço
esses backups locais uma vez por semana, assim eu não tenho que ter o trabalho
de apertar esse botãozinho todo dia 😉.

Vale a pena mencionar que a recuperação de backups também é muito fácil com o
borg. Existem dois comandos (oferecidos tanto pelo borg quanto pelo borgmatic):
``extract`` e ``mount``. ``extract`` recupera o(s) arquivo(s) de um dado
*archive*. **Note que ele vai ser salvo no mesmo caminho usado no archive,
possivelmente sobrescrevendo seus arquivos locais**, então melhor rodar ele
dentro de uma pasta temporária primeiro. ``mount`` monta o *archive* em uma
pasta local como se fosse um sistema de arquivos, permitindo que você navegue
pelos arquivos.

Conclusão
=========

Como você pode ver, eu melhorei significativamente meu sistema de backup. E isso
porque ele é menos interessante, não o contrário. Apesar de eu ter boas
lembranças de ter meu próprio sistema, que dependia de múltiplos script para
resolver os problemas com o que eu tinha, era um saco de manter. É ótimo ter
tudo funcionando de cara com o borg e o borgmatic: Eu consigo até esquecer que
eu estou fazendo backups agora, o que é um alívio.
