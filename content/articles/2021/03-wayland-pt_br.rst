######################
Mudando para o Wayland
######################

:date: 2021-03-21
:tags: wayland


No meio de janeiro, meu computador decidiu me surpreender, e não de um jeito
bom. Diferentemente de todas as peculiaridades que eu já estava acostumado nele
depois desses 6 anos de uso — teclando falhando, tela piscando, saída de áudio
com mal contato — dessa vez era pior, e nem era um problema de hardware.

No mínimo uma vez por dia, o computador travava completamente. A única ação que
surtia algum efeito era segurar o botão de desligar para forçá-lo a desligar.
Não exatamente o ideal...

Eu tive a idea de tentar entrar nele via SSH usando meu celular (ainda bem que o
Termux existe) e isso funcionou. Eu listei os processos e vi ``xorg-server``
usando 100% da CPU. Eu matei ele, e o computador magicamente voltou à vida. Mas
obviamente, já que eu acabei de matar a interface gráfica, todas minhas
aplicações abertas foram pro saco, e basicamente dava na mesma do que eu ter
reiniciado o computador.

Depois de três dias aguentando isso, tentando checar logs para depurar o
programa, eu desisti do X.Org.

Certamente a maioria de vocês sabem sobre isso, e eu com certeza não sou
qualificado para explicar, mas o X.Org, a aplicação que que serviu de base para
as interfaces gráficas no Linux por muito tempo é uma grande bagunça. O seu
código é muito difícil de manter, e a sua arquitetura apresenta problemas de
segurança, tanto que uma nova alternativa surgiu vários anos atrás chamada
Wayland_.

.. _Wayland: https://wayland.freedesktop.org/

A questão é que muitas aplicações hoje ainda dependem do X, o que também
significa que mudar para o Wayland pode ser uma mudança não muito suave. Então
apesar da mudança para o Wayland do ecossistema como um todo parecer inevitável,
as pessoas ainda estão confortáveis o suficiente no X.Org por enquanto.

E eu também era uma dessas pessoas, até que o próprio X me deu um motivo para
mudar. Esse problema provavelmente poderia ser investigado e consertado, mas
por que gastar tempo com `algo que vai entrar em modo de manutenção em breve`_?
Enfim chegou a hora de eu dar uma chance ao Wayland.

.. _algo que vai entrar em modo de manutenção em breve: https://www.phoronix.com/scan.php?page=news_item&px=X.Org-Maintenance-Mode-Quickly

Fazendo a mudança
=================

Do meu ponto de vista, mudar para o Wayland significava basicamente instalar ele
e mudar todas as aplicações que eu usava que dependiam do X, por equivalentes
que funcionassem no Wayland. A principal sendo a interface gráfica em si, que no
meu caso era o gerenciador de janelas i3_.

.. _i3: https://i3wm.org/

Felizmente, já existe um gerenciador de janelas bem estabelecido que é
compatível com o i3 para o Wayland, que é o sway_, então essa mudança foi bem
tranquila. Foi só questão de instalar o sway e mudar um pouco o arquivo de
configuração.

.. _sway: https://swaywm.org/

O que ajudou bastante foi que a wiki do sway tem um `guia de migração do i3`_,
que não só mostra as principais mudanças necessárias na configuração do i3 para
que ela funcione no sway, mas também uma lista dos programas Wayland
equivalentes aos normalmente usados no X.Org.

.. _guia de migração do i3: https://github.com/swaywm/sway/wiki/i3-Migration-Guide

Por exemplo, a definição e troca do layout de teclado, que normalmente é feita
pelo setxkbmap no X, é integrada ao sway então eu apenas usei comandos
``input`` no config do sway para configurar os layouts possíveis e ter um atalho
para alternar entre eles.

Ainda a respeito do teclado, eu me acostumei bastante a usar meu Caps Lock para
duas funções distintas: ele funciona como Esc quando é pressionado sozinho, mas
como Control quando é segurado enquanto outra tecla é apertada. Eu usava
setxkbmap junto com xcape para fazer isso, mas claramente eles não eram opções
no Wayland.

Quem me salvou foi o `Interception Tools`_, que permite que o mesmo seja feito
independente de estar no X ou no Wayland. Ele por si só apenas provê a
estrutura, mas essa função de Control e Esc no Caps Lock que mencionei pode ser
facilmente configurada usando o plugin oficial caps2esc_.

.. _Interception Tools: https://gitlab.com/interception/linux/tools
.. _caps2esc: https://gitlab.com/interception/linux/plugins/caps2esc

Ah, e ele também pode ser combinado com outro plugin, space2meta_, para usar a
barra de espaço como espaço quando apertada, mas como a tecla Meta (o
modificador no i3/sway) quando segurada. Eu já tinha experimentado isso no X
usando o xcape mas eu acabava disparando atalhos do i3 sem querer, então eu
parei de usar. Mas já que o space2meta é mais esperto e apenas manda a tecla no
evento da tecla solta, é bem usável e vale a pena pelo maior conforto.

.. _space2meta: https://gitlab.com/interception/linux/plugins/space2meta

Uma pequena tangente: Se o caps2esc e o space2meta parecem loucura para
você, eu realmente aconselho experimentar. Principalmente o caps2esc, já que
ele não tem nenhuma desvantagem, sinceramente. Pense bem, você realmente usa
tanto o Caps Lock? Não faz sentido mantê-lo na fileira central do teclado. Já o
Esc e o Control são muito úteis, ainda mais se você usa Vim. Bom, voltando...

E o rofi_, um menu de seleção, que eu uso para várias coisas diferentes? Também
depende do X. Existe uma alternativa para o Wayland chamado wofi_, mas eu não
gostei dele. Senti que ele é um pouco lento e não sou fã de ele ser GTK (o ícone
de lupa do lado do campo de entrada me incomoda bastante). Eu só quero um
retângulo de um única cor com um título, caixa de entrada e as opções filtradas
destacadas abaixo, e o rofi faz isso muito melhor. Por sorte, uma alma corajosa
criou um `fork do rofi para o Wayland`_ e ele funciona sem nenhum problema.
Praticamente...

.. _rofi: https://github.com/davatorium/rofi
.. _wofi: https://hg.sr.ht/~scoopta/wofi
.. _fork do rofi para o Wayland: https://github.com/lbonn/rofi

Infelizmente a funcionalidade de troca de janela não funciona nele. E eu acho
ela muito útil para pular diretamente para uma aplicação aberta, caso contrário
eu preciso passar por todos os workspaces procurando por ela. Mas foi bastante
fácil encontrar um script de troca de janela na internet, mas para o wofi, então
eu apenas mudei ele um pouco para funcionar com o rofi e assim consegui `meu
script de troca de janela`_. E agora sim, um rofi perfeito no Wayland.

.. _meu script de troca de janela: https://gist.github.com/nfraprado/484a0dfe60c49b7d2e9dfb45c3c4a4b5

Capturar a tela com o maim também não era mais possível. A alternativa é o
grim_, e um programa separado, slurp_, para permitir que uma região ou janela
seja selecionada para a captura. Como um bônus interessante, como o README do
grim mostra, é bem simples usá-lo, junto com o slurp e imagemagick, para obter a
cor do pixel embaixo do mouse, então eu também configurei esse modo. Minha
tentativa anterior de fazer isso com o maim falhou, então esse foi um bom bônus
de ter mudado para o Wayland.

.. _grim: https://github.com/emersion/grim
.. _slurp: https://github.com/emersion/slurp

xclip obviamente também não funciona no Wayland. Então para continuar tendo um
clipboard funcional, eu precisei instalar wl-clipboard_. Mas isso não foi o
suficiente para o Vim. Nele, operações no clipboard são feitas através do
registrador ``+``, por exemplo ``"+yy`` para copiar a linha atual para o
clipboard, mas isso não funciona com o clipboard do Wayland. Eu cheguei muito
perto de instalar o Neovim já que aparentemente isso funciona por padrão nele,
mas eu encontrei o plugin de Vim vim-wayland-clipboard_ que funciona
perfeitamente.

.. _wl-clipboard: https://github.com/bugaevc/wl-clipboard
.. _vim-wayland-clipboard: https://github.com/jasonccox/vim-wayland-clipboard

Uma coisa que eu não esperava que não funcionasse de cara era o compartilhamento
de tela. Felizmente `a página do PipeWire na Archwiki`_ fornece instruções de
como fazer funcionar, e é bem simples: apenas configurar uma variável de
ambiente e instalar `xdg-desktop-portal-wlr`_ e ``pipewire-media-session``. Dito
isso, eu tive um problema_ depois de uma atualização no PipeWire, mas ao que
tudo indica já foi resolvido.

.. _a página do PipeWire na ArchWiki: https://wiki.archlinux.org/index.php/PipeWire#WebRTC_screen_sharing
.. _xdg-desktop-portal-wlr: https://github.com/emersion/xdg-desktop-portal-wlr
.. _problema: https://gitlab.freedesktop.org/pipewire/pipewire/-/issues/768

Eu também mudei meu daemon de notificação do dunst para o mako_. Mas pelo que
vi, a partir da versão 1.6.0, o dunst também suporta Wayland, apesar de ainda
não ter sido empacotado no Arch Linux. Mas eu sinceramente também estou
satisfeito com o mako, então acho que vou continuar com ele.

.. _mako: https://github.com/emersion/mako

Algumas outras mudanças que eu acho que não precisam de nenhum comentário
adicional:

* light_ ao invés de xbacklight para controlar o brilho da tela
* gammastep_ ao invés de redshift para tornar a temperatura da tela mais
  agradável de noite
* imv_ ao invés de sxiv como o visualizador de imagens

.. _light: https://github.com/haikarainen/light
.. _gammastep: https://gitlab.com/chinstrap/gammastep
.. _imv:  https://github.com/eXeC64/imv

E finalmente, é importante mencionar o Xwayland. Ele é a aplicação que te
permite rodar aplicações X no Wayland. Não funciona para tudo (caso contrário eu
não teria mudado todas as aplicações anteriores), mas por exemplo para aplicações
Electron e para a Steam e jogos funcionou perfeitamente até agora, tanto que é
difícil saber que eles não são nativos no Wayland, o que é incrível. Se o
Wayland quer ser o futuro, ele precisa permitir que as aplicações X atuais
continuem rodando nele enquanto elas não são devidamente portadas para o
Wayland.

Plot twist
==========

Depois de todas essas mudanças eu estava totalmente estabelecido no Wayland, mas
ainda sim aconteciam alguns glitches gráficos. De tempo em tempo, as aplicações
começavam a piscar de jeitos estranhos e nada que eu fizesse fazia com que
parassem, a não ser, claro, fechar o Wayland. Então eu estava essencialmente de
volta ao ponto de partida, mas um pouco melhor já que nesse caso o computador
não travava completamente, então eu pelo menos tinha chance de salvar tudo antes
de reiniciar.

Felizmente, no meio-tempo eu recebi um computador novo da empresa em que estou
trabalhando, o que foi ótimo dadas as peculiaridades do meu computador antigo. E
já que eu já tinha me dado o trabalho de fazer a transição para o Wayland, eu
instalei sway nesse novo computador também. E tudo funcionou perfeitamente. Sem
problema algum.

Então, qual é a conclusão dessa história? O problema do congelamento/glitch
gráfico era algo relacionado ao hardware do meu computador antigo e não no X em
si, mas pelo menos isso me deu o empurrão que eu precisava para finalmente
migrar para o Wayland. Sim, eu provavelmente poderia ter continuado no X no
computador novo, mas isso seria apenas postergar o inevitável. O importante é
que agora tudo funciona, e eu me sinto muito mais confortável em depurar e
reportar problemas com a nova interface gráfica, dado que ela é bem mantida,
caso eles aconteçam no futuro.
