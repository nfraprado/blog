#################################
Resurrecting a computer on the go
#################################

:date: 2021-10-23
:tags: android, linux


Earlier this month I was spending a few weeks in another country. It was late at
night and I was once again looking over my personal files and thinking if there
was a better way to organize them in folders.

After thinking for a bit I decided on a new structure and moved some files
around. In the meantime, I realized I hadn't updated my packages for some time,
so I started an update.

At the end of the update I saw an error. ``/etc/mkinitcpio.conf`` was not found.
My ``/etc/mkinitcpio.conf`` is a symlink to a file in my home as this makes it
easier to track the changes I do in that config as part of my backups. And
between the files I had just moved was precisely that config, so I updated the
symlink to point to the new location. I ran the update once again and ``pacman``
reported "nothing to be done". I naively believed that everything was fine now.

So I rebooted to make sure that everything was working with the files in the new
place. As I selected my system in GRUB, it reported that
``/initramfs-linux.img`` was missing. Same for the fallback initramfs. I was
officially locked out of my system.

It was now clear to me what had happened. During the update the kernel was
updated, and as part of that the initramfs was re-generated, but since the
configuration file was invalid it was aborted. When I re-ran the update command,
``pacman`` only concluded that nothing had to be updated and said so. What I
should have done is to manually run the installation command again for the
kernel package to have the new initramfs generated. Since I didn't, there were
no initramfs files left on my boot partition, so I couldn't boot.

I took a deep breath and started thinking what I could do. I only needed an USB
flash drive with an ArchLinux ISO to boot from and re-run the kernel package
installation to fix that mess. Since I was in a different country though, my
equipment was limited. Thankfully I did have an USB flash drive, but no other
computer to flash the ISO from.

Unless... Unless I could use my beloved Nexus 5X phone to flash it. Its charging
port is an USB type C, and I suspected it worked as OTG as well. So I had the
USB flash drive and a USB A to type C dongle, I just had to discover if it was
possible to flash an ISO from the phone.

I immediately opened F-Droid and searched for an ISO flasher. To my happiness I
discovered EtchDroid_. Its description really resonated with me: "You can use it
to make a bootable GNU/Linux USB drive when your laptop is dead and you're in
the middle of nowhere.". Seeing it might actually be possible, I went to the
Arch Linux page and downloaded the ISO.

.. _EtchDroid: https://f-droid.org/en/packages/eu.depau.etchdroid/

I then installed EtchDroid and opened it. I selected the option to flash an ISO,
selected the ISO file, plugged the USB flash drive to the phone and selected it
in the app. I really liked the simple UI:

.. image:: {image}/etchdroid.png

I tapped the button to start flashing and a notification popped up showing the
progress:

.. image:: {image}/flash_in_progress.png

And after a few seconds it was done:

.. image:: {image}/flash_done.png

I then unplugged the USB from the phone and plugged it to my laptop. I was
really happy to see that the USB image worked and booted from it.

To fix the issue I set up my mountpoints, chrooted into my system, and did a
``pacman -S linux`` to reinstall the kernel package and have the initramfs
regenerated.

With the initramfs back in place, I rebooted and everything was working fine
again 🙂.

I'm really happy that despite my dumb mistake, I was able to solve it really
quickly even in a limited environment thanks to this nifty app called EtchDroid.
Definitely keeping this one around in case of future emergencies.
