##############################
Mudando o blog para o Codeberg
##############################

:date: 2022-08-29
:tags: blog, codeberg


Eu me importo bastante com FOSS, por isso sempre prefiro mudar para uma
alternativa FOSS quando uma nova surge, desde que não haja grandes desvantagens.

Na época da faculdade quando eu estava começando a usar o Git, eu só conhecia
duas opções de serviços de Git: GitHub, o mais conhecido mas proprietário, e o
GitLab, menos conhecido mas mais aberto. Entre os dois, o GitLab era a escolha
óbvia para os meus repositórios pessoais, incluindo esse blog.

Alguns meses atrás eu conheci o Codeberg_. O Codeberg provê uma instância
hospedada do Gitea_, que é uma plataforma para repositórios Git completamente
FOSS. Ainda por cima, o Codeberg é mantido por uma ONG, o que deixa claro que
ele é focado na comunidade, e não no dinheiro. Do meu ponto de vista, não tem
como ser melhor do que isso, então eu fiquei bastante motivado a mover todos
meus repositórios para o Codeberg.

.. _codeberg: https://codeberg.org/
.. _gitea: https://gitea.io/

A maioria dos meus repositórios são, na prática, simples backups: Eu sou o único
enviando código para eles, e desde que o histórico de commits esteja disponível,
eles não precisam de nenhuma outra funcionalidade. Por esse motivo a migração
foi bem simples. A única exceção é o repositório do blog.

Adaptação do blog para o Codeberg
=================================

A única grande funcionalidade faltante no Codeberg, e da qual eu dependia no
GitLab para o blog, é a CI.

Minha configuração atual do blog no GitLab era de ter um repositório com CI
configurada para que toda vez que eu enviasse novas mudanças, a CI rodasse o
pelican para gerar as páginas do site e servisse elas.

No Codeberg, como não há CI, quaisquer arquivos que estiverem presentes no
repositório especial ``pages`` são servidos diretamente. Esse método exigiu
que eu criasse dois repositórios: um para os arquivos fonte, o `repositório
blog`__, e outro para a saída gerada ser servida, o `repositório pages`__.

.. __: https://codeberg.org/nfraprado/blog
.. __: https://codeberg.org/nfraprado/pages

**Observação**: `Também é possível`__ usar um branch ``pages`` no mesmo
repositório, mas a opção do repositório separado me pareceu mais organizada.

.. __: https://codeberg.page/

Mas claro que eu não queria gerenciar o repositório adicional ``pages``, já que
isso seria irritante e rapidamente me faria sentir falta da CI do GitLab. Por
isso eu adicionei um novo comando ``make deploy`` no Makefile para automatizar o
processo de publicar o site no repositório ``pages``. Ele pode ser visto `nesse
commit <commit make deploy_>`__.

.. _commit make deploy: https://codeberg.org/nfraprado/blog/commit/44bb31065898a942164a8a23c526d9363c750d93

O que o ``make deploy`` faz é:

* Fazer os passos anteriormente feitos no `.gitlab-ci.yml`_ para gerar os
  arquivos de saída do blog, especificamente:

  * Gerar as strings de tradução (através do ``make trans_deploy``)
  * Gerar as páginas do site (através do ``make publish``)

* Commitar os arquivos de saída e fazer push deles para o repositório ``pages``

.. _.gitlab-ci.yml: https://codeberg.org/nfraprado/blog/src/commit/38cc4cfa0b5bb34eb5d088210abbce5cbf553ff7/.gitlab-ci.yml

Já que os arquivos de saída gerados pelo pelican precisam ser commitados e
enviados para o repositório ``pages``, a pasta local de saída dentro da pasta do
blog agora precisa ser um repositório git. Para permitir isso, a variável
``OUTPUT_RETENTION`` `precisou ser atualizada <commit make deploy_>`__ para que
o pelican não delete a pasta ``.git`` toda vez que ele rodar.

Em seguida eu melhorei as mensagens de commit para o repositório ``pages``
através `desse commit`__, formatando elas com tanto o hash quanto a descrição,
`no mesmo formato`__ que é usado para as tags ``Fixes:`` no kernel Linux. Isso
me permite não apenas correlacionar todo commit no repositório ``pages`` ao
commit correspondente no repositório ``blog``, mas também facilmente identificar
sobre o que foram as últimas mudanças.

.. __: https://codeberg.org/nfraprado/blog/commit/38cc4cfa0b5bb34eb5d088210abbce5cbf553ff7
.. __: https://www.kernel.org/doc/html/latest/process/submitting-patches.html#describe-your-changes

`Eu também adicionei`__ um arquivo ``.domains`` à retenção já que ele é
necessário para usar um domínio customizado com o Codeberg.

.. __: https://codeberg.org/nfraprado/blog/commit/19f8d66f86038744362b6c9d23a33607e0b8ac84

Conclusão
=========

No começo a necessidade de versionar os arquivos de saída com Git me pareceu
desagradável, mas o fato de ser em um repositório separado e ser fácil
relacionar os commits aos fontes fez com que isso deixasse de ser um problema
para mim.

Inclusive, eu percebi duas vantagens nessa estratégia do repositório ``pages``
em relação a ter uma CI. Primeiro, eu não preciso mais pensar nas versões de
imagens e pacotes que estão sendo usados na CI porque não há CI. Desde que meu
sistema local seja capaz de gerar o blog, tudo continuará funcionando (e eu já
precisava ter uma configuração local de toda forma para conseguir testar as
mudanças antes de enviar). Segundo, assim que eu rodo ``make deploy`` as
mudanças são aplicadas instantaneamente no site, enquanto na CI do GitLab
levariam em torno de um minuto para atualizar.

Então foram necessárias algumas pequenas mudanças para ter uma boa configuração
do blog no Codeberg mas eu estou feliz com a mudança. O Codeberg parece um ótimo
novo lar e eu vou fazer questão de cuidar bem dele 🙂.
