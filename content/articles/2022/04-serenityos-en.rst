########################
Learning from SerenityOS
########################

:date: 2022-04-29
:tags: serenityos


One day I was scrolling through Reddit as usual, when I saw a post linking to
this blog post: `I quit my job to focus on SerenityOS full time`_. I was
intrigued by the backstory, the premise of this OS, and also by the fact that
its development was `being recorded on Youtube`__.

.. _I quit my job to focus on SerenityOS full time: https://awesomekling.github.io/I-quit-my-job-to-focus-on-SerenityOS-full-time/
.. __: https://www.youtube.com/c/AndreasKling/videos

Now, almost one year later, I have watched almost every video Andreas has posted
in his channel since and a few of the older ones too. His videos are so good,
they're entertaining, inspiring and I've learned so much from them. And he's
such a nice human being too. The fact that such a positive project came out of
his own therapy is just perfect. I think he definitely found his calling in life
and I'm really happy for him.

SerenityOS is such an interesting project itself. There's code for the kernel,
libraries, services and applications, all there in a single repository. It
invites you to become familiar with the whole system and to see how each
component interacts with the others and fits with the whole. Furthermore there's
such a huge variety on what kind of problems each component is trying to solve
that it feels like there's always something interesting to explore on Serenity.

But having everything together isn't only helpful for learning the code base.
When you make the whole system, it's possible to do some really cool
integrations. Some of my favorites features on Serenity are:

* All data exposed by the kernel through procfs-like files is in JSON!
* The Profile Viewer app is not only able to show system-wide profiles with
  tree-view stack traces but also even signposts set by traced applications
  during interesting events!
* There's a nice markup language to design the GUI for the applications, GML,
  and the GML Playground app shows you a live preview of the GUI as you write!
* On *every* application (unless it opted-out) it's possible to open a command
  pallete with all commands on that app!

Another advantage that comes from making the entire stack is that changing APIs
is much easier, since they control both sides. That allows faster iteration and
for better ways to do things to evolve over time without needing to maintain
older iterations for compatibility concerns. That said, that flexibility doesn't
extend to the ABI and interactions with the user. And although so far it has
been the case that these haven been freely changed as well, I wonder how much of
this is due to SerenityOS still being seen as Work-In-Progress and there not
being daily users yet. But perhaps it being "a system by us, for us", the
ability to iterate and improve upon even the ABI will be seen as more important
than keeping it stable. I guess we'll see.

When it comes to contributing to the project myself, so far I've done just a
couple ones, mainly `the most basic support for showing album covers in the
sound player`__ and `adding proper support for multiple keyboard layouts in the
KeyboardSettings app`__. But I had a lot of fun working on them, and look
forward to contributing more.

.. __: https://github.com/SerenityOS/serenity/pull/12841
.. __: https://github.com/SerenityOS/serenity/pull/12764

**Edit**: on the next day from publishing this post my contribution on Sound
Player was showcased in `SerenityOS's April Update video`__! 😃

.. __: https://youtu.be/yUmHEYs5n34?t=2105

One thing that really got me thinking is Serenity's goal of having 1990's-like
GUI. When I started to use and learn about Linux some years ago, I was
immediately attracted by CLI and TUI programs and have preferred them since.
They represented a very different paradigm from the GUI programs that I was used
to in the Windows world. On the terminal there was no fear of exposing all the
functionalities in a very flexible manner, there was much more room for
customizations and performance tended to be better.

But after seeing Serenity's approach, I think my disregard for GUI applications
has been a bit misguided. With the right mindset, GUI programs can be quite
flexible, customizable and performant. And on top of that they can look
consistent and be aesthetically pleasing. What else do you want from software?
In short, I'm actually confident now that I'd prefer using GUIs for most tasks,
it's just that the ones out there don't tend to focus on the right things for
me.

SerenityOS still does feel a bit like a playground. And it is fun to play around
with. But I also agree with a lot of its goals, and it is really rapidly
growing, so it is a very promising OS to me. On one hand, I wish it was ready
for everyday use so I could switch to it already, but on the other, that would
take all the fun of getting there (both when contributing myself and also seeing
Andreas' videos!). Plus it's easier to contribute to it if there are still a lot
of rough edges 🙂.
