###########################
Aprendendo com o SerenityOS
###########################

:date: 2022-04-29
:tags: serenityos


Um dia eu estava navegando pelo Reddit como de costume, quando eu vi uma
postagem linkando para esse artigo: `I quit my job to focus on SerenityOS full
time`_. Eu fiquei intrigado pela história, pela premissa desse SO, e também pelo
fato do seu desenvolvimento estar sendo `gravado no Youtube`__.

.. _I quit my job to focus on SerenityOS full time: https://awesomekling.github.io/I-quit-my-job-to-focus-on-SerenityOS-full-time/
.. __: https://www.youtube.com/c/AndreasKling/videos

Agora, quase um ano depois, eu assisti quase todos os vídeos que o Andreas
publicou no canal desde então e alguns dos mais antigos também. Os vídeos dele
säo muito bons, eles são divertidos, inspiradores e eu aprendi muito com eles. E
ele é uma pessoa muito boa também. O fato de um projeto tão positivo ter surgido
da própria terapia dele é demais. Na minha opinião ele definitivamente achou seu
propósito na vida e eu estou muito feliz por ele.

O SerenityOS é um projeto extremamente interessante por si só. Ele contém código
para o kernel, bibliotecas, serviços e aplicações, tudo em um único repositório.
Ele te convida a se familizarizar com o sistema inteiro e ver como cada
componente interage com os outros e se encaixa no todo. Além do mais existe uma
variedade tão grande nos tipos de problemas que cada componente está tentando
resolver que parece que sempre tem algo interessante para explorar no Serenity.

Mas ter tudo no mesmo lugar não é útil só para aprender sobre o código. Quando
você faz o sistema inteiro, é possível fazer algumas integrações muito legais.
Algumas das minhas funcionalidades preferidas do Serenity são:

* Todos os dados expostos pelo kernel através de arquivos no estilo procfs são
  no formato JSON!
* O aplicativo Profile Viewer não só é capaz de mostrar perfis de desempenho do
  sistema inteiro com as pilhas de execução em árvore como também até
  identificadores colocados pelas aplicações amostradas para demarcar eventos
  interessantes!
* Existe uma bela linguagem de marcação para descrever a GUI dos aplicativos,
  GML, e o aplicativo GML Playground mostra uma visualização em tempo real
  conforme você escreve!
* Em *todas* aplicações (a não ser que explicitamente desabilitado) é possível
  abrir uma paleta de comandos, com todos os comandos da aplicação.

Outra vantagem em fazer o sistema inteiro é que mudar APIs é muito mais fácil,
já que eles controlam ambos os lados. Isso permite iterar mais rapidamente e que
melhores formas de fazer as coisas evoluam com o tempo sem haver necessidade de
manter as iterações anteriores para manter a retrocompatibilidade. Dito isso,
essa flexibilidade não se estende para a ABI e interações com o usuário. E
apesar de até o momento essas também terem sido alteradas livremente, eu não sei
o quanto disso é apenas devido ao SerenityOS ainda ser visto como em
desenvolvimento e não haverem usuários de verdade. Mas talvez sendo um
sistema "por nós, para nós" como diz o README, a habilidade de melhorar até
mesmo na ABI pode ser vista como mais importante do que mantê-la estável. O
tempo dirá.

Quanto às minhas contribuições, até agora eu fiz apenas algumas, principalmente,
`o suporte mais básico possível para mostrar capas de álbum no Sound Player`__ e
`adição de suporte para múltiplos layouts de teclado no aplicativo Keyboard
Settings`__. Mas eu me diverti bastante trabalhando nelas, e estou ansioso para
contribuir mais.

.. __: https://github.com/SerenityOS/serenity/pull/12841
.. __: https://github.com/SerenityOS/serenity/pull/12764

**Atualização**: no dia seguinte da publicação desse artigo a minha contribuição
no Sound Player foi mostrada no `vídeo de atualização do SerenityOS de Abril`__!
😃

.. __: https://youtu.be/yUmHEYs5n34?t=2105

Uma coisa que realmente me fez pensar foi o objetivo do Serenity de ter uma GUI
no estilo de 1990. Quando eu comecei a usar e aprender sobre Linux alguns anos
atrás, eu imediatamente fui atraído pelos programas CLI e TUI e preferi eles
desde então. Eles representavam um paradigma bem diferente dos programas GUI que
eu estava acostumado no mundo Windows. No terminal não havia medo de expor todas
as funcionalidades de uma maneira bem flexível, existia muito mais espaço para
customização e a performance tendia a ser melhor.

Mas depois de ver a abordagem do Serenity, eu acho que meu desprezo por
aplicações GUI tem sido equivocado. Com a mentalidade certa, programas GUI podem
sim ser bastante flexíveis, customizáveis e ter boa performance. E além disso
podem ter uma aparência consistente e estética agradável. O que mais você quer
de um software? Em resumo, eu agora estou confiante de que eu prefiriria usar
GUIs para a maioria das tarefas, é só que as que estão por aí não tendem a focar
nas coisas certas para mim.

O SerenityOS ainda passa a sensação de um brinquedo. E é bem divertido brincar
com ele. Mas eu também concordo com muitos dos seus objetivos, e ele está
crescendo muito rápido, portanto é um SO bem promissor para mim. Por um lado, eu
gostaria que ele já estivesse pronto para o uso cotidiano para que eu pudesse
mudar para ele logo, mas por outro, isso tiraria toda a diversão de chegar lá
(tanto em contribuir eu mesmo quanto assistir aos vídeos do Andreas!). Fora que
é mais fácil contribuir quando ainda tem bastante coisa para fazer 🙂.
