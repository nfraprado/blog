###########################
Moving the blog to Codeberg
###########################

:date: 2022-08-29
:tags: blog, codeberg


As someone who cares about FOSS, I'm always happy to move to a FOSS alternative
when one shows up, provided there aren't any big drawbacks.

Back when I was in University and starting to learn the ways of Git, I only knew
about two Git hosting options: GitHub, the mainstream but proprietary, and
GitLab, the less known but more open alternative. Between the two, GitLab was
the obvious choice for my personal repositories, including this blog.

A few months ago I learned about Codeberg_. Codeberg provides a hosted instance
of Gitea_, which is a Git forge that is entirely FOSS. On top of that, Codeberg
is backed by a non-profit, which makes it clear that it is community-focused. As
far as I'm concerned, it doesn't get better than this, so I was eager to move
all my repositories to Codeberg.

.. _codeberg: https://codeberg.org/
.. _gitea: https://gitea.io/

Most of my repositories are really just archives: I'm the only one pushing code
to them, and as long as the commit history is available, there's no other
feature they require. So the migration was pretty straight-forward. The only
exception is the blog repository.

Codeberg blog setup
===================

The one big missing feature in Codeberg which I relied on in GitLab for my blog
is the CI.

My current setup for the blog in GitLab was to have a repository with the CI
configured so that whenever I pushed new changes, the CI would run pelican to
generate the static pages for the website and serve them.

On Codeberg, since there's no CI, whatever files are present in the special
``pages`` repository are directly served. This approach required me to set up
two repositories: one for the source files, the `blog repository`__, and another
for the generated output to be served, the `pages repository`__.

.. __: https://codeberg.org/nfraprado/blog
.. __: https://codeberg.org/nfraprado/pages

**Note**: `It's also possible`__ to use a ``pages`` branch in the same repository to
serve the files, but the separate repository approach seemed neater to me.

.. __: https://codeberg.page/

But of course I didn't want to manage the extra ``pages`` repository, since that
would be annoying and quickly make me miss GitLab's CI. So I added a new ``make
deploy`` target in the Makefile to automate the process of deploying the site to
the ``pages`` repo. It can be seen in `this commit <make deploy commit_>`__.

.. _make deploy commit: https://codeberg.org/nfraprado/blog/commit/44bb31065898a942164a8a23c526d9363c750d93

What ``make deploy`` does is:

* Do the steps previously done in the `.gitlab-ci.yml`_ to generate the blog
  output, namely:

  * Generate the translation strings (through ``make trans_deploy``)
  * Generate the static site pages (through ``make publish``)

* Commit the output files and push them to the ``pages`` repository

.. _.gitlab-ci.yml: https://codeberg.org/nfraprado/blog/src/commit/38cc4cfa0b5bb34eb5d088210abbce5cbf553ff7/.gitlab-ci.yml

Since the output files generated from pelican need to be commited and pushed to
the ``pages`` repo, the local output directory inside the blog directory now
needs to be a git repository. To acommodate for that, the ``OUTPUT_RETENTION``
variable `needed updating <make deploy commit_>`__ so that pelican doesn't
delete the ``.git`` folder every time it runs.

I subsequently improved the commit messages for the ``pages`` repo through
`this commit`__, by formatting them with both the commit hash and description,
in `the same format`__ that is used for ``Fixes:`` tags in the Linux kernel.
This allows me not only to correlate any commit in the ``pages`` repo to the
corresponding commit in the ``blog`` repo, but also easily tell from a glance
what the latest changes were about.

.. __: https://codeberg.org/nfraprado/blog/commit/38cc4cfa0b5bb34eb5d088210abbce5cbf553ff7
.. __: https://www.kernel.org/doc/html/latest/process/submitting-patches.html#describe-your-changes

`I also added`__ a ``.domains`` file to the retention since it is required by
Codeberg for using a custom domain.

.. __: https://codeberg.org/nfraprado/blog/commit/19f8d66f86038744362b6c9d23a33607e0b8ac84

Conclusion
==========

At first the need to version output files with Git put me off, but the fact that
it's in a separate repository and it's easy to correlate the commits to the
source ones made it a non-issue for me.

I did notice a couple advantages on the ``pages`` repo approach over CI. First,
I don't need to think about the image and package versions I'm using for the CI
because there's no CI. As long as my local system is able to generate the blog,
I'm good (and I already needed to have a working local setup to test changes
before pushing anyway). Second, as soon as I ``make deploy``, the new changes
are live, while GitLab's CI would have taken a minute or so to update.

So it took some little changes to have a good blog setup on Codeberg but I'm
happy with the move. Codeberg feels like a nice new home and I'll be sure to
take good care of it 🙂.
