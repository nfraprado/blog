########################################################
Encontrando com a comunidade Linux na minha primeira ELC
########################################################

:date: 2022-09-29
:tags: conferência


Esse mês eu participei da Embedded Linux Conference Europe, que ocorreu junto
com a Open Source Summit Europe, em Dublin, na Irlanda. Essa foi minha primeira
conferência de Linux presencial como contribuidor e foi uma experiência muito
especial.

Mas essa não foi minha primeira conferência de Linux presencial na vida. Em 2018
eu participei da linuxdev-br, que ocorreu justamente na Unicamp, onde eu estava
fazendo graduação. Eu me lembro de ter assistido a apresentação do Sergio Prado
sobre como escrever um driver de LED e ter ficado fascinado com o fato de um LED
físico poder ser ligado e desligado simplesmente escrevendo em um arquivo.

Essa conferência foi um daqueles momentos que mudam sua vida dali para frente,
já que foi lá que eu conheci a Helen Koike, que me convidou para participar dos
encontros semanais do LKCAMP_, onde eles estavam ensinando sobre como o kernel
funciona e ajudando as pessoas a submeterem seus primeiros *patches* e se
tornarem contribuidoras. E depois de aprender bastante com eles e ter algumas
contribuições aceitas eu eventualmente entrei na Collabora.

.. _LKCAMP: https://lkcamp.dev/

De volta para 2022 e aqui estava eu em Dublin, para minha segunda conferência de
Linux presencial na vida, dessa vez uma bem maior, e tendo trabalhado por mais
de um ano na Collabora, participando como um contribuidor ativo do kernel Linux.

Como um contribuidor, eu estou acostumado a interagir com outros
desenvolvedores do kernel através das listas de email no dia-a-dia. E apesar de
isso funcionar eficientemente para o projeto, é difícil se sentir conectado de
uma forma humana com a comunidade lidando apenas com respostas em texto e
endereços de email.

E é por isso que encontrar com essas pessoas na vida real foi tão revigorante. É
uma ótima mudança de ritmo poder conversar com elas de uma forma mais expressiva
e humana, não apenas sobre trabalho mas sobre a vida no geral. E também de fato
poder vê-las com meus próprios olhos e confirmar que são pessoas de verdade,
com suas próprias vidas, aspirações, preferências de comida. Me lembrou que o
mundo inteiro realmente é construido por pessoas como eu.

Também foi minha primeira vez encontrando com meus colegas de trabalho da
Collabora, o que foi espetacular! Foto aqui__. Sou eu ali na esquerda 🙂.

.. __: https://twitter.com/Collabora/status/1570761841089073153

A Linux Foundation publicou o álbum de fotos do evento aqui__ caso queira ver.

.. __: https://www.flickr.com/photos/linuxfoundation/albums/72177720301869820

Eu mal voltei da viagem e já mal posso esperar pela próxima oportunidade de ver
todo mundo de novo!

.. image:: {image}/convention-center-night.jpg

(O centro de convenções de Dublin iluminando a noite, do outro lado do rio
Liffey)
