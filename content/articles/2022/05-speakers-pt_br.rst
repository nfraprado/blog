#######################################
Descobrindo o conforto de alto-falantes
#######################################

:date: 2022-05-27
:tags: compra, conforto, som


Em 2014, durante uma viagem escolar para a Alemanha, eu comprei um fone de
ouvido Razer Tiamat. O som era ótimo e no geral era bem confortável. O único
problema era que ele apertava bastante a cabeça e me dava dor de cabeça depois
de longas horas de uso.

Esse fone me serviu bastante e eu usei ele até 2021. A essa altura, o couro das
almofadas já tinha desgastado quase completamente e o conector p2 quebrou e
precisou soldar um novo duas vezes. Uma das vezes o contato de um dos fios não
estava muito bom, o que fez com que um lado ficasse mais alto que o outro e
precisou ser compensado em software, mas a compensação necessária mudava
conforme o volume mudava, o que me deixou doido e eu lembro de ter ficado
absurdamente feliz quando eu finalmente consertei isso.

Então em 2021 já estava mais do que na hora de um novo fone. Já que a pressão na
cabeça era o meu maior problema, eu queria achar um que apertasse o mínimo
possível e que ainda tivesse um preço razoável.

À procura de um fone confortável
================================

Pesquisando na internet eu encontrei um site chamado Rtings que analiza e
classifica fones, e um dos critérios é o quanto ele aperta (*Clamping force*).
Eu filtrei a lista por esse critério e decidi pegar o HyperX Cloud Alpha já que
ele era um dos que menos apertava e não era muito caro.

Mas quando ele chegou, eu fiquei bem decepcionado. Ele apertava muito mais do
que o Tiamat e me dava dor de cabeça bem rápido depois de uma ou duas horas de
uso. Eu tentei deixar ele esticando de um dia para o outro, mas não fez nenhuma
diferença. Então eventualmente eu comecei a evitar a usar o fone e usar os
alto-falantes embutidos do notebook, que soam muito mal, mas pelo menos não
machucam.

Alguns meses depois eu tinha me mudado para os EUA e tinha uma gama muito maior
de opções à disposição, então eu decidi tentar de novo. Eu não queria errar uma
segunda vez, então eu fiz questão de experimentar o maior número possível de
fones em lojas físicas, e a B&H era a maior que eu conhecia.

Mas para minha surpresa, quase todos eles apertavam o suficiente para serem
desconfortáveis para mim. Até o Bose QuietComfort 35, que pelas minhas pesquisas
era um dos fones mais confortáveis, era um pouco apertado para mim. Mas eu achei
sim alguns dos fones confortáveis, como o Sony Mdr7506, Phillips HP9500 e Koss
UR20. E curioso que eles eram mais baratos que a maioria também. Mas esses fones
eram tão soltos que parecia que iriam cair facilmente. Aparentemente eu não
conseguia achar um meio termo para a pressão do fone.

Nessa hora eu comecei a pensar que talvez meu problema era inerente a fones. Eu
já tinha pensado em fones intra-auriculares também, mas o problema deles é que
eles deformam dentro do seu ouvido, causando outro tipo de dor. Então de repente
eu pensei em alto-falantes. Será que eles seriam a minha solução?

Procurando um bom alto-falante
==============================

Pesquisando eu encontrei `um ranking de alto-falantes bem pesquisado e testado
da Wirecutter`__. Aparentemente essa é uma série bem conhecida de rankings, mas
eu nunca tinha ouvido falar então fiquei feliz de encontrar.

.. __: https://www.nytimes.com/wirecutter/reviews/best-computer-speakers/

Eu cheguei bem perto de comprar um dos falantes mais recomendados deles, mas eu
senti que seria mais importante ter um subwoofer do que a resposta em frequência
ser plana como eles estavam priorizando, já que meu caso de uso é basicamente
ouvir música, e não produzir.

Eles tinham como "Also great" o Klipsch ProMedia 2.1 THX, que tem subwoofer.
Pesquisando em outros rankings, eu percebi que apesar dos primeiros colocados
serem diferentes, esse falante estava consistentemente aparecendo na maioria
deles. E aparentemente ele é vendido já há uns bons anos, então se ele continua
relevante nesses rankings, deve realmente ser bom.

As desvantagens do Klipsch geralmente mencionavam a necessidade de espaço extra
para o subwoofer espaçoso e a falta de conectividade com por exemplo bluetooth -
a entrada de áudio é somente através de um cabo p2. Eu não vi nenhum problema
quanto ao espaço: o subwoofer vai no chão, e lá eu tenho espaço de sobra! Quanto
à entrada de áudio: eu ia deixar o falante conectado constantemente na porta
"Line Out" da doca do meu notebook, então sem problemas também.

Por fim, para ter uma ideia melhor do quão importante um subwoofer seria para
mim, eu abri algumas músicas que eu gosto e que tem bastante grave no Audacity e
usei o efeito "Filter Curve" e tentei imitar o gráfico de resposta em frequência
que o artigo da Wirecutter mostrou para um dos falantes que não tinha subwoofer
para ver como soaria. Isso se mostrou útil e ficou claro que sem as baixas
frequências, essas músicas perderam bastante substância. Então eu concluí que
realmente queria um subwoofer, e o Klipsch parecia ser o melhor falante com um,
então eu escolhi ele.

O Klipsch ProMedia 2.1 THX
==========================

E olha... Essa deve ter sido uma das minhas compras mais felizes na vida.
Primeiro de tudo, por causa da mudança de fone para alto-falante. Eu nem
acredito que finalmente posso ouvir música *o dia inteiro* sem sentir
*absolutamente nada* na minha cabeça. Isso é *mágico*.

Segundo, o subwoofer realmente traz vida para a música. Não é *toda* música que
faz uso dele, mas mais músicas do que eu pensei que seria. Dá para ouvir (e
sentir!) ele principalmente no baixo e na percussão das músicas. O Klipsch tem
um ajuste de volume separado para o subwoofer e eu fico diminuindo e aumentando
ele para comparar a diferença que faz e realmente é impressionante.

Quanto às desvantagens, o Klipsch tem um LED bem brilhante para mostrar que
está ligado, mas não tem um botão para desligar, ou seja, esse LED fica sempre
ligado. Mas um pedacinho de fita isolante em cima resolve o problema.

Outra coisa é que agora que eu tenho um alto-falante tão bom preso na minha
mesa, eu sinto falta dele quando levo meu notebook para um outro cômodo. Se
tivesse um jeito de trazer ele comigo de uma forma portátil seria ótimo... Mas
aí voltamos ao conceito de fone de ouvido e já sabemos onde esse caminho dá 😝.
Eu também sinto um pouco de falta do isolamento sonoro (nos dois sentidos) de um
fone, mas isso é bem menos importante do que conforto para mim.

Conclusão
=========

Eu acho interessante que quando eu era criança, o computador na minha casa tinha
um par de alto-falantes como era de costume. Mas desde então eu só tive, e vi
outras pessoas usarem, fones de ouvido, então eu incoscientemente passei a
considerá-los como a única opção. Perceber que esse não é o caso, apesar de
alto-falantes hoje em dia aparentemente serem bem menos comuns, e voltar a
usá-los é até poético de certa forma.

Eu ainda tenho meu odiado fone de ouvido e eu uso ele para chamadas, já que o
eco dos alto-falantes seria muito ruim. Mas assim que a chamada termina eu fico
*muito* feliz em tirar o fone e voltar para o conforto dos meus alto-falantes 😌.
