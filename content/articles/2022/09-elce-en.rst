###########################################
Meeting the Linux community at my first ELC
###########################################

:date: 2022-09-29
:tags: conference


This month I attended the Embedded Linux Conference Europe, which was co-located
with the Open Source Summit Europe, and took place in Dublin, Ireland. This was
my first time attending a Linux conference in-person as a contributor and it was
a very special experience.

This wasn't my first ever in-person Linux conference though. Back in 2018 I
attended linuxdev-br. It took place right at Unicamp, where I was an
undergraduate student. I remember watching Sergio Prado's presentation on how to
write an LED driver and being fascinated with the fact that a physical LED could
be turned on and off by simply writing to a file.

That conference was actually a major turning point in my life, as it was there
that I first met Helen Koike, who invited me to attend the weekly meetings for
LKCAMP_, where they were teaching about the kernel inner workings and guiding
people to submit their first patches and become contributors. And after learning
a lot from them and having some contributions merged I eventually made my way
into Collabora.

.. _LKCAMP: https://lkcamp.dev/

Fast-forward to 2022 and here I was in Dublin, for my second ever in-person
Linux conference, this time at a much bigger one, and having worked for
more than a year at Collabora, as an active contributor to the Linux kernel.

As a contributor, I'm used to interacting with the other kernel developers
through the mailing lists on a daily basis. And while that works efficiently for
the project, dealing with just text replies and email aliases can feel very
distant.

And that's why meeting these people in real life was so reinvigorating. It's a
very nice change of pace to be able to talk to them in a more expressive and
human manner, not just about work but life in general. And to actually see with
my own eyes that these are all still real people, with their own life,
aspirations, food preferences. It reminded me that the whole world really is
built by people just like me.

It was also my first time meeting with my colleagues at Collabora, which was
absolutely amazing! Picture here__. That's me all the way on the left 🙂.

.. __: https://twitter.com/Collabora/status/1570761841089073153

There's a photo album of the event from the Linux Foundation here__ in case you
want to check it out.

.. __: https://www.flickr.com/photos/linuxfoundation/albums/72177720301869820

I barely got back from this trip and I already can't wait for the next
opportunity to meet everyone again!

.. image:: {image}/convention-center-night.jpg

(The Convention Centre Dublin glowing bright at night, on the other
side of the Liffey river)
