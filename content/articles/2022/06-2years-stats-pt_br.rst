###################################
Estatísticas do blog após dois anos
###################################

:date: 2022-06-20
:tags: aniversário-blog, blog


Uau, fazem dois anos desde que eu comecei esse blog! Estou muito feliz que
consegui manter o ritmo de um artigo por mês pelo segundo ano. Espero que
consiga manter pelo terceiro!

Para esse artigo eu pensei que seria divertido ver algumas estatísticas sobre o
que eu fiz no blog até agora. Para isso eu `criei um plugin 'stats'`__ para o
pelican que gera as estatísticas em que estou interessado, e `escrevi
scripts em python usando o matplotlib`__ para gerar gráficos dos dados
coletados.

.. __: https://codeberg.org/nfraprado/blog/commit/c372aa64ce2049d42df0df4f908955905e89411b
.. __: https://codeberg.org/nfraprado/blog/commit/f6c10780301d53661eff67052e180b9705d94610

Gráficos e estatísticas
=======================

Eu consegui manter o ritmo mensal de publicação, mas não consegui sempre
publicar no dia 20 do mês como eu pretendia, e isso fica aparente nesse gráfico:

.. image:: {image}/dom-publish_br.png

(Abra a imagem em uma nova aba para ver melhor)

No começo eu ainda não tinha decidido uma data para publicar todo mês, mas
começando com o artigo de setembro de 2020 eu decidi que seria o dia 20 e
consegui manter essa meta, quase sempre, até maio do ano seguinte. Depois disso
eu praticamente nunca mais publiquei no dia 20, e existe até uma certa tendência
ascendente a partir daí, com os artigos deslizando cada vez mais para o fim dos
meses.

Esse outro gráfico mostra a quantidade total acumulada em cada dia do mês:

.. image:: {image}/dom-publish-count_br.png

O dia 20 ainda é o dia em que eu mais publiquei, com 8 no total, mas a maior
parte dos artigos estão distribuídos praticamente uniformemente nos dias após
ele, totalizando 17! Ou seja, só um terço dos artigos, aproximadamente, foram
de fato publicados no dia 20 como eu queria.

Mas isso não me incomoda muito. Seria legal ter mantido a publicação dos artigos
consistentes no mesmo dia do mês, mas publicar todo mês independente do dia é
muito mais importante para mim, e eu estou bem feliz que tive sucesso nisso.

Mudando um pouco de assunto, eu sempre quis saber a quantidade de palavras de
cada artigo, então aqui está:

.. image:: {image}/word-count_br.png

Não existe nenhuma tendência óbvia, o que era de se esperar. O menor artigo de
todos foi o `<{article}mbsync-oauth2>`__ com 553 palavras em inglês e 567
palavras em português, enquanto o maior de todos foi o `<{article}vit>`__ com
surpreendentes 2727 palavras em inglês e 2747 palavras em português. O segundo
maior artigo foi o `<{article}kindle-jailbreak>`__ não muito abaixo, enquanto os
demais ficaram todos abaixo das 2000 palavras.

Esse gráfico também permite comparar a quantidade de palavras entre inglês e
português, e fica claro que a versão em português é quase sempre um pouco maior.
Isso confirma a minha percepção, já que várias vezes eu lembro de ter aumentado
o tamanho da frase tentando expressar em português algo que em inglês eu
consegui expressar com um único termo técnico comum.

No total, todos os artigos em inglês somam 32718 palavras, enquanto todos os em
português somam 33488 palavras. Logo, meus artigos em português são em média
2% maiores, o que honestamente é menos do que eu esperava.

Importante mencionar que essa contagem de palavras é feita na etapa final de
geração do artigo, quando ele já está em sua forma HTML, o que significa que
todo o conteúdo que aparece nele após publicado está lá, com a diferença de que
as tags HTML que poderiam adicionar à contagem são removidas. Na prática isso
significa que a contagem total de palavras do artigo é o texto mais os blocos de
código. Portanto um elemento importante que não aparece nesse gráfico são as
imagens, que podem ser vistas a seguir:

.. image:: {image}/num-images_br.png

Nem todos os artigos tem imagens, mas alguns deles tem muitas delas! Inclusive,
o `<{article}kindle-jailbreak>`__, que é o segundo maior artigo, é o que possui
mais imagens, com 16 no total!

Essa parte é bem arbitrária, mas eu queria levar em consideração o número de
imagens na comparação de quantidade de conteúdo entre artigos. Eles dizem que
uma imagem vale mais que mil palavras, mas no meu caso isso parece um pouco
exagerado. Ao invés disso, contar cada imagem como 75 palavras, um parágrafo
normal, parece mais razoável. Além disso, decidi fazer GIFs valerem três vezes
mais, 225 palavras, já que o fato de serem animados os torna muito mais
ricos. Com isso em mente, eu gerei um gráfico com a estimativa de conteúdo total
de todos os artigos:

.. image:: {image}/total-content_br.png

Mais uma vez, os números são totalmente subjetivos, e isso ainda não leva em
consideração os arquivos de áudio que eu compus e linkei no
`<{article}melodies>`__, nem os códigos que eu escrevi em algum repositório e
que apenas linkei no blog, como no caso do `<{article}pcmn>`__. Mas dadas as
limitações, eu diria que essa estimativa está muito mais próxima da minha
percepção de quanto conteúdo cada artigo possui.

Uma notável diferença é que nesse novo gráfico, o
`<{article}kindle-jailbreak>`__ se tornou o artigo com mais conteúdo.

Enfim, de volta a métricas mais objetivas, eu gerei um gráfico com o número de
links para outros artigos, ou referências cruzadas, que cada artigo possui:

.. image:: {image}/num-article-links_br.png

A maioria dos artigos não faz referência a outros (esperado), e os que fazem,
são no máximo duas. Dito isso, este artigo apareceria muito mais alto do que o
restante nesse gráfico, mas eu diria que ele é uma exceção, já que é um artigo
instrospectivo sobre o blog.

E links quaisquer?

.. image:: {image}/num-links_br.png

Todo artigo tem pelo menos um link, o que não é de se espantar, essa é a
internet afinal de contas!

E finalmente, a pergunta mais importante, quantos emojis??

.. image:: {image}/num-emojis_br.png

Não tão comuns quanto links (felizmente?), mas também nada mal. Aparentemente eu
estou ficando  mais consistente nos meus emojis, o que quer que isso signifique.

Mas de que vale saber quantos emojis estão sendo usados se não pudermos ver
quais?

.. image:: {image}/top-emoji_br.png

Claro que o sorriso é o mais comum, difícil não sorrir quando se está falando de
algo em que está interessado. No segundo lugar temos o rosto com a língua para
fora para os momentos engraçados. E mais alguns emojis aleatórios.

Esse gráfico foi bem mais difícil de gerar do que parece, mas mais sobre ele no
mês que vem (provavelmente).

Conclusão
=========

Apesar de ser necessário um certo esforço para conseguir escrever sobre algo
todo mês, eu realmente gosto do resultado. Esse ritual mensal se mostrou
terapêutico para mim, já que me dá a oportunidade de construir um novo tijolo
nesse espaço seguro que é o meu blog, então eu sinto que estou avançando em algo
na minha vida mesmo quando mais nada acontece.

A necessidade de um artigo todo mês também me incentiva a continuar fazendo as
coisas que me interessam, porque se não eu não vou ter sobre o que escrever!
