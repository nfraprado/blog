########################################
Collecting pins
########################################

:date: 2023-11-23
:tags: pin


It started innocently about one year ago as I was looking for a gift for a
friend. We played a lot of League of Legends and Ragnarok Online back in the
day, so I wanted to give him something to remember that. Eventually I came
across these pins:

* `Poring/Poporing from Ragnarok Online`__
* `Vi's Gauntlet from League of Legends`__

.. __: https://www.etsy.com/listing/587004849/poring-poporing-slime-monter-fanart-hard
.. __: https://www.etsy.com/listing/1280961309/arcane-enamel-pin-fanmade-vis-hextech

They were exactly what I was looking for so I bought them. That was going to be
it, but I am still personally a huge fan of Ragnarok Online, and those pins were
so cute that I thought, I should buy them for me too!

But after that I couldn't stop looking for more pins. I was hooked! I kept
browsing Etsy, searching for pins from games I like, and favoriting them so I
could find them later.

I also started looking for pins at every merchandise store I came by, which I'd
never given much attention previously. I now actually had something of interest
to buy! So after a few weeks I got a small collection:

.. image:: {image}/pins-before-pax.jpg

Around that time I was thinking about attending `PAX east`_. When I found out the
event attracted a big community of pin collectors and hosted pin trading events,
from `this video`__, I knew I had to go.

.. _PAX east: https://en.wikipedia.org/wiki/PAX_(event)
.. __: https://www.youtube.com/watch?v=7hoSSI6A-_U

In order to be able to conveniently bring my pins to PAX, and to bring back the
ones I would get there, I bought a `PinFolio Classic`_.

.. _PinFolio Classic: https://www.amazon.com/dp/B07YYR3J1V/ref=cm_sw_r_as_gl_api_gl_i_NJTTQQ2YXTGC585NB7YN

On my first day at PAX I didn't hold back and bought every nice pin I saw:

.. image:: {image}/pins-bought-1st-day-pax.jpg

After lucking out and winning two golden cassette pins in a row from a carnival
wheel that was setup there, I went to the pin trading room to trade some pins
and meet new people. It was fun to recognize some of the same people that showed
up on the video.

With PAX over, this was my collection:

.. image:: {image}/pinfolio-after-pax.jpg

(the Deviruchi pin is missing since it was pinned to my beanie)

Now, many months later I have finally filled up a display case with my
collection. Note that I have some more pins, but I plan to use those for
trading in future events, so they're stored in the pinfolio. So without further
ado, here is my collection, labeled:

.. image:: {image}/pins-full-labeled.jpg

1. Poring. From Ragnarok Online. Fan-made (Teaberryhouse). The one that started it all! `Link <https://www.etsy.com/listing/587004849/poring-poporing-slime-monter-fanart-hard>`__
2. Poporing. See 1.
3. ``/gg`` Emote. Came with 4.
4. Deviruchi. From Ragnarok Online. Fan-made (FinniChang). Bought together with 1 and 2. `Link <https://www.etsy.com/listing/742804941/deviruchi-gg-ragnarok-online-enamel-pin>`__
5. Eevee. From Pokemon. Fan-made (Jackalandhare). I was buying two pins for a friend that loves
   Pokemon and the third was cheaper, so I got one for myself as well. `Link
   <https://www.etsy.com/listing/729815460/hard-enamel-pins-eeveelution-eevee?variation0=1222773687>`__
6. Fairy bottle. From Zelda. Fan-made (FinniChang). `Link <https://www.etsy.com/listing/842366471/fairy-bottle-enamel-pin?variation0=1473891219>`__
7. Van Gogh's The Starry Night. Got it while visiting the Museum of Modern Art
   in NYC, where they keep the real painting.
8. Prince & Katamari. From Katamari Damacy. I played Katamari Damacy Reroll last
   year and thought it was really cool! And who can resist a spinning pin?
   Bought at the Fangamer stand at PAX. `Link
   <https://www.fangamer.com/collections/katamari-damacy/products/katamari-damacy-the-prince-katamari-spinning-pin>`__
9. King. See 8. `Link <https://www.fangamer.com/collections/katamari-damacy/products/king-of-all-cosmos-pin-spinning>`__
10. Ravenclaw. From Harry Potter. I bought it at a Harry Potter store in the
    London airport, where I had a connection. I don't really care about
    Ravenclaw, but it was the best-looking pin there.
11. Junimos. From Stardew Valley. Bought at the Fangamer stand at PAX. `Link <https://www.fangamer.com/collections/stardew-valley/products/pinverse-junimos-pin-pack>`__
12. Came with 11.
13. Babaa (Apparently that's its name!). From Neopets. I played a lot of Neopets
    when I was younger, so when I saw these available at a Geekify stand at PAX,
    I just had to buy some.
14. Strawberry & Chocolate Paintbrush. See 13.
15. Wraith Paintbrush. See 13.
16. Fight Button. From Undertale. Big Undertale fan, as will be obvious from
    this list, so I just went crazy. Bought at Fangamer stand at PAX. `Link <https://www.fangamer.com/collections/undertale/products/undertale-mercy-or-fight-enamel-pin-set>`__
17. Mercy Button. Came with 16.
18. Annoying dog. See 16. `Link <https://www.fangamer.com/collections/undertale/products/undertale-annoying-dog-lapel-pin>`__
19. Wonderville Arcade machine. Bought while visiting Wonderville_ with a
    friend, which is really a unique and awesome place!
20. Psychokinetic Raz. From Psychonauts 2. I haven't played the game yet, just
    the first one, but I had just finished watching the `PsychOdyssey
    documentary`_ and was blown away so I had to buy something to remember it.
    Bought at Fangamer stand at PAX. `Link <https://www.fangamer.com/products/raz-psychonauts-pin-spinning>`__
21. Phantom of the Opera. Bought it on the way out after watching the play on
    Broadway. It was great!
22. Canada Wilderness. Gifted from a friend. He bought it during our trip to
    Toronto (at the store on the base of the tower, I believe)
23. Loto. Gifted from a friend. It's an old one got from the second hand market,
    unlike all the others in this collection. I love how it has actual numbers
    for each tiny square.
24. Frisk. From Undertale. Bought at Fangamer stand at PAX. `Link <https://www.fangamer.com/collections/undertale/products/undertale-character-pins-set-1>`__
25. Toriel. Came with 24.
26. Sans. Came with 24.
27. Flowey. Came with 24.
28. Papyrus. See 24. `Link <https://www.fangamer.com/collections/undertale/products/undertale-character-pins-set-2>`__
29. Undyne. Came with 28.
30. Alphys. Came with 28.
31. Mettaton. Came with 28.
32. Mask. From Mr Robot. Bought it second-hand online. It's from a 2016 Loot
    Crate. I really wish it was smaller.
33. Software Freedom Conservancy's Logo. Gifted from a friend.
34. L's Logo. From Death Note. Bought at a geek store in Antwerp.
35. Dwarf. From Dwarf Fortress. One of the reasons I went to PAX was to meet the
    creators of the game in person (and I did!). Shortly after a Dwarf Fortress
    pin was announced and I was sure to buy one online. `Link <https://www.fangamer.com/products/dwarf-fortress-pin-finely-crafted>`__
36. PAX East VHS. Bought at PAX. People at PAX will mostly only trade Pinny
    Arcade pins, so I bought this as a part of a 4-pin set to kick start my
    collection and be able to trade at the event.
37. East 2023 Logo. Came with 36.
38. Diamond Ore Block. From Minecraft. Got this from trading at PAX. Minecraft
    was one of the most influential games in my life, so I was super happy!
39. ATLAS and P-Body. From Portal 2. Got this from trading at PAX. I love
    Portal, but this is my least favorite pin out of `the set
    <https://pinnydb.com/pinDetail/271>`__, so I hope to trade it for one of
    others next PAX.
40. Raz. From Psychonauts. I was given this one for free by someone at the
    trading room at PAX. She said she already had too many pins and since I was
    just starting out and liked this one, she was kind enough to give it to me
    🙂.
41. ? Block. From Super Mario Bros. Bought at PAX as part of a 4-pin set to
    start my Pinny Arcade collection and be able to trade. `Link
    <https://store.penny-arcade.com/collections/pins/products/super-mario-bros-1-1-pin-set>`__
42. Mario. Came with 41.
43. Fairly OddParents. I watched tons of hours of this cartoon, so was really
    happy to find a pin for it. To my knowledge the only official pin. Made by
    Zen Monkey Studios. `Link <https://www.ebay.com/itm/234773032756>`__
44. Paranaguá train. Gifted from a friend, who took that train. I really need to
    go there one day wearing this pin 😃.
45. Kerbal Space Program. Gifted from the same person as 40.
46. Green Flame. From Acquisitions Inc. It's a tabletop RPG campaign played live
    during PAX. I watched it during PAX and got this pin to remember the moment.
    `Link <https://store.penny-arcade.com/collections/pins/products/green-flame-pin>`__
47. Mae. From Night in the Woods. I bought it at PAX before even playing the
    game. I just *really* loved the style. Possibly my favorite pin.
48. Los Andes. Gifted from a friend who visited that place, and he's really into
    rock climbing so it's very fitting.
49. Goose. From Kickstarter. During PAX the Kickstarter folks had geese
    scattered across the stands of each project funded by kickstarter. If you
    found all of them you would get a pin. It was incredibly hard but I did it.
50. PAX Gold Mixtape. During PAX there was a carnival wheel that you could pay
    Pinny Arcade pins to spin and get one of the mixtape pins. I was extremely
    lucky and managed to get two of the golden ones. I traded one and kept this
    one.
51. Portuguese tile. Bought during a trip to Portugal.
52. Collabora's logo. Got it during the company meetup. Unfortunately it's
    magnetic, so I'm too scared to wear it on the streets and lose it.
53. Ecuador. Gifted from a friend who visited that place, the same rock climber
    as 48, see a pattern? 😝

.. _Psychodyssey documentary: https://www.youtube.com/watch?v=kRlI72bsNRc&list=PLIhLvue17Sd70y34zh2erWWpMyOnh4UN_
.. _wonderville: https://www.wonderville.nyc/

So far I've been keeping my pins in a display case on a side table. I've
thought about having them on a board on the wall to make them more visible but
I'm worried they might get damaged over time being constantly exposed like
that.

However I don't like to just leave my pins locked up in a case either, since
that seems like a waste, so I wear them. I've found what works best for me is
to wear them on a beanie. (See first photo) As a result I can't wear them
during Summer (I'm still looking for an alternative here). I always keep it to
a single pin, since more than that looks too noisy to me, and switch it quite
frequently and when possible to match the occasion.

Now that this display case is full I'll need to buy a new one (or maybe I'll
try a board this time). I'll probably make another post when that one gets full
too, though that might take a while. I do have many pins on my wishlist, but
I'm trying not to go too crazy with it. It's about the quality not the quantity
after all, and I don't want it to get to a point where I can't appreciate every
single one.
