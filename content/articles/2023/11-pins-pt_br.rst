########################################
Colecionando pins
########################################

:date: 2023-11-23
:tags: pin


Começou inocentemente mais ou menos um ano atrás quando eu estava procurando um
presente para um amigo. A gente jogou muito League of Legends e Ragnarok Online
nos tempos de escola, então eu queria dar uma recordação disso para ele.
Eventualmente eu encontrei esses pins:

* `Poring/Poporing do Ragnarok Online`__
* `Luva da Vi do League of Legends`__

.. __: https://www.etsy.com/listing/587004849/poring-poporing-slime-monter-fanart-hard
.. __: https://www.etsy.com/listing/1280961309/arcane-enamel-pin-fanmade-vis-hextech

Eles eram exatamente o que eu estava procurando então eu comprei. Era para
acabar aí, mas eu ainda sou muito fã do Ragnarok Online, e esses pins eram tão
fofos que eu pensei, eu deveria comprar para mim também!

Mas depois disso eu não conseguia mais parar de procurar por pins. Eu estava
viciado! Eu continuei navegando no Etsy, pesquisando pins de jogos que eu gosto,
e favoritando eles para que eu conseguisse encontrá-los depois.

Eu também comecei a procurar pins em toda loja de souvenirs, que nunca tinham me
chamado atenção, que eu passava. Agora eu realmente tinha algo de interessante
para comprar! Então depois de algumas semanas eu já estava com uma pequena
coleção:

.. image:: {image}/pins-before-pax.jpg

Nessa época eu estava pensando em ir para a `PAX east`_. Quando eu descobri que
o evento atraia uma grande comunidade de colecionadores de pin e contava com
eventos de troca de pins, através `desse vídeo`__, eu percebi que eu tinha que
ir.

.. _PAX east: https://en.wikipedia.org/wiki/PAX_(event)
.. __: https://www.youtube.com/watch?v=7hoSSI6A-_U

Para conseguir levar meus pins convenientemente para a PAX, e trazer de volta os
que eu conseguisse lá, eu comprei um `PinFolio Classic`_.

.. _PinFolio Classic: https://www.amazon.com/dp/B07YYR3J1V/ref=cm_sw_r_as_gl_api_gl_i_NJTTQQ2YXTGC585NB7YN

No meu primeiro dia na PAX eu não me segurei e comprei todos os pins que eu
gostei:

.. image:: {image}/pins-bought-1st-day-pax.jpg

Depois de ter muita sorte e ganhar dois pins de fita cassete dourada em seguida
de uma roleta, eu fui para a sala de troca de pins para trocar alguns pins e
conhecer pessoas novas. Foi divertido reconhecer algumas das mesmas pessoas que
apareceram no vídeo.

Ao final da PAX, essa era minha coleção:

.. image:: {image}/pinfolio-after-pax.jpg

(o pin do Deviruchi está faltando porque estava preso na minha touca)

Agora, muitos meses depois eu finalmente completei uma caixa com a minha
coleção. Eu tenho mais alguns outrs pins, mas a minha intenção é usá-los para
trocas em eventos futuros, então eles estão guardados no pinfolio. Então sem
mais delongas, aqui está minha coleção, numerada:

.. image:: {image}/pins-full-labeled.jpg

1. Poring. Do Ragnarok Online. Feito por fã (Teaberryhouse). O primeiro da coleção! `Link <https://www.etsy.com/listing/587004849/poring-poporing-slime-monter-fanart-hard>`__
2. Poporing. Veja 1.
3. ``/gg`` Emote. Veio com 4.
4. Deviruchi. Do Ragnarok Online. Feito por fã (FinniChang). Comprado junto com
   1 e 2. `Link <https://www.etsy.com/listing/742804941/deviruchi-gg-ragnarok-online-enamel-pin>`__
5. Eevee. Do Pokemon. Feito por fã (Jackalandhare). Eu estava comprando dois
   pins para um amigo que ama Pokemon e o terceiro era mais barato, então eu
   comprei um para mim também. `Link <https://www.etsy.com/listing/729815460/hard-enamel-pins-eeveelution-eevee?variation0=1222773687>`__
6. Fairy bottle. Do Zelda. Feito por fã (FinniChang). `Link <https://www.etsy.com/listing/842366471/fairy-bottle-enamel-pin?variation0=1473891219>`__
7. A Noite Estrelada de Van Gogh. Comprei enquanto visitava o Museu de Arte
   Moderna em NYC, onde eles mantêm o quadro real.
8. Príncipe e Katamari. Do Katamari Damacy. Eu joguei Katamari Damacy Reroll ano
   passado e achei muito legal! E quem consegue resistir a um pin que gira?
   Comprado no stand da Fangamer na PAX. `Link <https://www.fangamer.com/collections/katamari-damacy/products/katamari-damacy-the-prince-katamari-spinning-pin>`__
9. Rei. Veja 8. `Link <https://www.fangamer.com/collections/katamari-damacy/products/king-of-all-cosmos-pin-spinning>`__
10. Corvinal. Do Harry Potter. Eu comprei em uma loja do Harry Potter no
    aeroporto de Londres, onde eu tinha uma conexão. Eu não ligo para Corvinal,
    mas era o pin mais bonito da loja.
11. Junimos. Do Stardew Valley. Comprado no stand da Fangamer na PAX. `Link <https://www.fangamer.com/collections/stardew-valley/products/pinverse-junimos-pin-pack>`__
12. Veio com 11.
13. Babaa (Esse é o nome aparentemente!). Do Neopets. Eu joguei muito Neopets
    quando eu era mais novo, então quando vi esses pins disponíveis no stand da
    Geekify na PAX, eu tive que comprar.
14. Pincel de Morango e Chocolate. Veja 13.
15. Pincel de Espectro. Veja 13.
16. Botão Fight. Do Undertale. Eu sou muito fã do Undertale como deve ter ficado
    óbvio pelas fotos anteriores. Comprado no stand da Fangamer na PAX. `Link <https://www.fangamer.com/collections/undertale/products/undertale-mercy-or-fight-enamel-pin-set>`__
17. Botão Mercy. Veio com 16.
18. Annoying dog. Veja 16. `Link <https://www.fangamer.com/collections/undertale/products/undertale-annoying-dog-lapel-pin>`__
19. Máquina de Fliperama do Wonderville. Comprei enquanto visitava o
    Wonderville_, que é um lugar único e incrível, com um amigo.
20. Psychokinetic Raz. Do Psychonauts 2. Eu ainda não joguei o jogo, só o
    primeiro, mas eu tinha acabado de terminar de assistir o `documentário
    PsychOdyssey`_ que é incrível e então comprei esse pin como recordação.
    Comprado no stand da Fangamer na PAX. `Link <https://www.fangamer.com/products/raz-psychonauts-pin-spinning>`__
21. Fantasma da Ópera. Comprei na saída do espetáculo do Fantasma da Ópera na
    Broadway. Foi muito bom!
22. Canada Wilderness. Ganhei de um amigo. Ele comprou durante uma viagem nossa
    para Toronto (acho que na loja da torre).
23. Loto. Ganhei de um amigo. É um pin antigo, diferente de todos os outros
    nessa coleção. Eu acho legal que ele tem números para cada um dos minúsculos
    quadradinhos.
24. Frisk. Do Undertale. Comprado no stand da Fangamer na PAX. `Link <https://www.fangamer.com/collections/undertale/products/undertale-character-pins-set-1>`__
25. Toriel. Veio com 24.
26. Sans. Veio com 24.
27. Flowey. Veio com 24.
28. Papyrus. Veja 24. `Link <https://www.fangamer.com/collections/undertale/products/undertale-character-pins-set-2>`__
29. Undyne. Veio com 28.
30. Alphys. Veio com 28.
31. Mettaton. Veio com 28.
32. Máscara. Do Mr Robot. Comprei usado da internet. É de uma Loot Crate de
    2016. Queria muito que ele fosse menor.
33. Logo da Software Freedom Conservancy. Ganhei de um amigo.
34. Logo do L. Do Death Note. Comprei em uma loja geek em Antwerp.
35. Anão. Do Dwarf Fortress. Um dos motivos de eu ter ido para a PAX foi para
    conhecer os criadores do jogo pessoalmente (e eu conheci!). Logo depois um
    pin de Dwarf Fortress foi anunciado e eu comprei online no mesmo dia. `Link <https://www.fangamer.com/products/dwarf-fortress-pin-finely-crafted>`__
36. VHS PAX East. Comprado na PAX. As pessoas na PAX praticamente só trocam
    pins que sejam Pinny Arcade, então eu comprei esse como parte de um conjunto
    de 4 pins para começar minha coleção e poder trocar no evento.
37. Logo da PAX East 2023. Veio com 36.
38. Bloco de Minério de Diamante. Do Minecraft. Consegui esse trocando na PAX.
    Minecraft foi um dos jogos com maior impacto na minha vida, então eu fiquei
    muito feliz de conseguir ele.
39. ATLAS e P-Body. Do Portal 2. Consegui ele trocando na PAX. Eu amo Portal,
    mas esse é o pin que eu menos gostei do `set
    <https://pinnydb.com/pinDetail/271>`__, então eu quero tentar trocar ele por
    um dos outros na próxima PAX.
40. Raz. Do Psychonauts. Eu ganhei esse de graça de uma pessoa na sala de trocas
    da PAX. Ela disse que já tinha muitos pins e já que eu estava começando
    minha coleção e gostei dele, eu podia ficar com ele 🙂.
41. Bloco ?. Do Super Mario Bros. Comprado na PAX como parte de um conjunto de 4
    para começar minha coleção Pinny Arcade e conseguir trocar. `Link <https://store.penny-arcade.com/collections/pins/products/super-mario-bros-1-1-pin-set>`__
42. Mario. Veio com 41.
43. Padrinhos mágicos. Eu assisti muuitas horas desse desenho no Jetix quando
    era criança. Até onde eu sei o único pin oficial, feito pelo Zen Monkey
    Studios. `Link <https://www.ebay.com/itm/234773032756>`__
44. Trem de Paranaguá. Presente de um amigo que andou nesse trem. Agora eu
    preciso ir lá um dia usando esse pin 😃.
45. Kerbal Space Program. Ganhei da mesma pessoa que 40.
46. Green Flame. Da Acquisitions Inc. É uma campanha de RPG de mesa jogada ao
    vivo durante a PAX. Eu assisti na PAX e comprei esse pin de recordação. `Link <https://store.penny-arcade.com/collections/pins/products/green-flame-pin>`__
47. Mae. Do Night in the Woods. Eu comprei ele na PAX antes de ter jogado o
    jogo. Eu simplesmente amei o estilo artístico. Provavelmente meu pin
    preferido.
48. Los Andes. Ganhei de um amigo que visitou esse lugar, e ele adora escalar,
    então achei bem apropriado.
49. Ganso. Do Kickstarter. Durante a PAX, o Kickstarter espalhou placas de
    ganso pelos stands de cada projeto financiado pelo Kickstarter. Se você
    encontrasse todos você ganhava um pin. Foi muito difícil mas eu consegui.
50. Fita de ouro da PAX. Durante a PAX você podia pagar pins Pinny Arcade para
    girar uma roleta e ganhar a fita que caisse. Eu fui extremamente sortudo e
    consegui duas fitas de ouro seguidas. Eu troquei uma e fiquei com a outra.
51. Azulejo. Comprei durante uma viagem para Portugal.
52. Logo da Collabora. Ganhei durante o encontro da empresa. Infelizmente é
    magnético, então eu tenho medo de usar na rua e perder.
53. Equador. Presente de um amigo que visitou esse lugar, o mesmo escalador que
    48, percebeu o padrão? 😝

.. _documentário Psychodyssey: https://www.youtube.com/watch?v=kRlI72bsNRc&list=PLIhLvue17Sd70y34zh2erWWpMyOnh4UN_
.. _wonderville: https://www.wonderville.nyc/

Até então eu tenho guardado meus pins em uma caixa com tampa de vidro que fica
em cima de uma mesa. Eu já pensei em deixar eles em um quadro na
parede para que eles ficassem mais visíveis, mas fiquei preocupado de acabarem
estragando com o tempo por estarem expostos o tempo todo.

Mas eu também não deixo eles trancados na caixa permanentemente, porque isso
seria um desperdício, então eu uso eles na roupa. Especificamente na touca, que
é onde funciona melhor para mim (Veja a primeira foto). Por esse motivo eu não
consigo usar pins durante o verão (ainda estou pensando em uma alternativa). Eu
sempre uso um único pin porque acho que mais que isso fica bagunçado, e mudo
frequentemente e tento combinar com a ocasião.

Agora que essa caixa está cheia eu vou precisar comprar uma nova (ou talvez
tentar um quadro dessa vez). Eu provavelmente vou publicar outro artigo quando
esse encher também, mas isso provavelmente vai levar um tempo. Eu já tenho
uma longa lista de pins para comprar, mas estou tentando maneirar. O foco é a
qualidade e não a quantidade, e eu não quero chegar em um ponto onde não consigo
apreciar cada um deles.
