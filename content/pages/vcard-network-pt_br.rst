#############
vCard Network
#############

:status: hidden

Veja `esta página <https://codeberg.org/nfraprado/vcard-network>`__ para mais
informações sobre como conectar comigo usando o vCard abaixo.

Perfil
======

.. include:: ../../extra/vcard-network/profile-br.rst

Conexões
========

.. include:: ../../extra/vcard-network/connections-br.rst
