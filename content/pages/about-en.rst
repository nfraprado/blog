#####
About
#####

:icon: info-circle


Welcome to my personal blog where I write about anything that catches my
interest.

About me
========

`My profile and connections <vcard-network.html>`__.

Links
=====

My code: https://codeberg.org/nfraprado

----

*"Explore, learn, share."*
