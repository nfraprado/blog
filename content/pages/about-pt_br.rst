#####
Sobre
#####

:icon: info-circle


Esse é meu blog pessoal onde eu escrevo sobre qualquer coisa que acho
interessante.

Sobre mim
=========

`Meu perfil e minhas conexões <vcard-network.html>`__.

Links
=====

Meu código: https://codeberg.org/nfraprado

----

*"Explorar, aprender, compartilhar."*
