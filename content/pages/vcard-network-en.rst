#############
vCard Network
#############

:status: hidden

See `this page <https://codeberg.org/nfraprado/vcard-network>`__ for information
on how to connect with me using the vCard below.

Profile
=======

.. include:: ../../extra/vcard-network/profile-en.rst

Connections
===========

.. include:: ../../extra/vcard-network/connections-en.rst
