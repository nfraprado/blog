class id3art(object):

    def get_attribute(self, file):
        import eyed3
        artist = eyed3.load(file.relative_path).tag.artist
        return str(artist) if artist else ''
    
    
    def get_change_attribute_cmd(self, file, old, new):
        from ranger.ext.shell_escape import shell_escape as esc
        return "eyeD3 -a %s %s" % (esc(new), esc(file))

class id3tit(object):

    def get_attribute(self, file):
        import eyed3
        title = eyed3.load(file.relative_path).tag.title
        return str(title) if title else ''
    
    
    def get_change_attribute_cmd(self, file, old, new):
        from ranger.ext.shell_escape import shell_escape as esc
        return "eyeD3 -t %s %s" % (esc(new), esc(file))

class id3alb(object):

    def get_attribute(self, file):
        import eyed3
        album = eyed3.load(file.relative_path).tag.album
        return str(album) if album else ''
    
    
    def get_change_attribute_cmd(self, file, old, new):
        from ranger.ext.shell_escape import shell_escape as esc
        return "eyeD3 -A %s %s" % (esc(new), esc(file))

bulk = {'id3art': id3art(),
        'id3tit': id3tit(),
        'id3alb': id3alb(),
        }
