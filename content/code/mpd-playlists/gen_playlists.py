#!/bin/python
from os.path import expanduser

from mpd_playlist import MPDPlaylist
import playlists

PLD = expanduser("~/.config/mpd/playlists")

# All variables defined locally with type MPDPlaylist and with a name will be
# contained here
playlists = (pl for pl in vars(playlists).values()
             if isinstance(pl, MPDPlaylist) and pl.name)

for playlist in playlists:
    playlist.write_to_file(PLD)
