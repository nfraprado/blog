PY?=python3
PELICAN?=pelican
PELICAN_OPTS=

DIR_BASE=$(CURDIR)
DIR_INPUT=$(DIR_BASE)/content
DIR_OUTPUT=$(DIR_BASE)/output
DIR_THEME=$(DIR_BASE)/theme

CFG_MAIN=$(DIR_BASE)/cfg_main.py
CFG_PUBLISH=$(DIR_BASE)/cfg_publish.py
CFG_STATS=$(DIR_BASE)/cfg_stats.py


DEBUG ?= 0
ifeq ($(DEBUG), 1)
	PELICAN_OPTS += -D
endif

RELATIVE ?= 0
ifeq ($(RELATIVE), 1)
	PELICAN_OPTS += --relative-urls
endif

ifdef PORT
	PORT_OPT=-p $(PORT)
endif

help:
	@echo 'Makefile for a pelican Web site                                                       '
	@echo '                                                                                      '
	@echo 'Usage:                                                                                '
	@echo '                                                                                      '
	@echo '  Common workflow:                                                                    '
	@echo '    make html                           (re)generate the web site                     '
	@echo '    make serve [PORT=8000]              serve site at http://localhost:8000           '
	@echo '    make devserver [PORT=8000]          serve and regenerate as needed                '
	@echo '    make publish                        generate using production settings            '
	@echo '    make deploy                         deploy site: Publish and push output to repo  '
	@echo '                                                                                      '
	@echo '  Changing translations:                                                              '
	@echo '    make trans-update                   update translation files from templates       '
	@echo '    make trans-deploy                   apply translation files                       '
	@echo '                                                                                      '
	@echo '  Misc.:                                                                              '
	@echo '    make clean                          remove the generated files                    '
	@echo '    make stats                          generate blog statistics                      '
	@echo '    make vcard-network                  generate vcard-network pages                  '
	@echo '                                                                                      '
	@echo 'Set the DEBUG variable to 1 to enable debugging, e.g. make DEBUG=1 html               '
	@echo 'Set the RELATIVE variable to 1 to enable relative urls                                '
	@echo '                                                                                      '

html:
	$(PELICAN) $(DIR_INPUT) -o $(DIR_OUTPUT) -s $(CFG_MAIN) $(PELICAN_OPTS)

clean:
	rm -rf $(DIR_OUTPUT)/*

serve:
	$(PELICAN) -l $(DIR_INPUT) -o $(DIR_OUTPUT) -s $(CFG_MAIN) $(PELICAN_OPTS) $(PORT_OPT)

devserver:
	$(PELICAN) -lr $(DIR_INPUT) -o $(DIR_OUTPUT) -s $(CFG_MAIN) $(PELICAN_OPTS) $(PORT_OPT)

publish: vcard-network
	$(PELICAN) $(DIR_INPUT) -o $(DIR_OUTPUT) -s $(CFG_PUBLISH) $(PELICAN_OPTS)

_deploy-pages-repo:
	git -C $(DIR_OUTPUT) add .
	git -C $(DIR_OUTPUT) commit -m "Deploy $$(git log --pretty='%h ("%s")' -n 1)"
	git -C $(DIR_OUTPUT) push

deploy: trans-deploy publish _deploy-pages-repo

trans-update:
	cd $(DIR_THEME) && \
	pybabel extract --mapping babel.cfg --output messages.pot ./ && \
	pybabel update --input-file messages.pot --output-dir translations/ --domain messages

trans-deploy:
	cd $(DIR_THEME) && \
	pybabel compile --directory translations/ --domain messages

stats:
	$(PELICAN) $(DIR_INPUT) -o $(DIR_OUTPUT) -s $(CFG_STATS) $(PELICAN_OPTS)

vcard-network:
	awk 1 ~/ark/etc/dav/contacts/5DEB-65D21680-2F1-4AEA3480/* | vcard-render -t extra/vcard-network/template-connections-en.rst > extra/vcard-network/connections-en.rst
	awk 1 ~/ark/etc/dav/contacts/5DEB-65D21680-2F1-4AEA3480/* | vcard-render -t extra/vcard-network/template-connections-br.rst > extra/vcard-network/connections-br.rst
	cat content/profile.vcf | vcard-render -t extra/vcard-network/template-profile-en.rst > extra/vcard-network/profile-en.rst
	cat content/profile.vcf | vcard-render -t extra/vcard-network/template-profile-br.rst > extra/vcard-network/profile-br.rst

.PHONY: html help clean serve devserver publish trans-update trans-deploy stats deploy vcard-network
