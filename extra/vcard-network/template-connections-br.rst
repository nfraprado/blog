{% for vcard in vcards %}
* `{{in_lang_preferred(vcard.fn, "pt-BR")[0].value}} <{{in_lang_preferred(vcard.url, "pt-BR")[0].value}}>`__: `vCard <{{in_lang_preferred(vcard.source, "pt-BR")[0].value}}>`__
{% endfor %}
