{% for vcard in vcards %}
.. image:: {{vcard.photo[0].value}}

* **Name**: {{in_lang_preferred(vcard.fn, "en-US")[0].value}}
* **Nickname**: {{in_lang_preferred(vcard.nickname, "en-US")[0].value}}
* **Organization**: {{' - '.join(in_lang_preferred(vcard.org, "en-US")[0].value)}}
* **Address**: {{in_lang_preferred(vcard.adr, "en-US")[0].value.city}}
* **Email**: {{in_lang_preferred(vcard.email, "en-US")[0].value}}
* **Additional notes**: {{in_lang_preferred(vcard.note, "en-US")[0].value}}

`vCard <{{vcard.source[0].value}}>`__

{% endfor %}
