{% for vcard in vcards %}
.. image:: {{vcard.photo[0].value}}

* **Nome**: {{in_lang_preferred(vcard.fn, "pt-BR")[0].value}}
* **Apelido**: {{in_lang_preferred(vcard.nickname, "pt-BR")[0].value}}
* **Organização**: {{" - ".join(in_lang_preferred(vcard.org, "pt-BR")[0].value)}}
* **Endereço**: {{in_lang_preferred(vcard.adr, "pt-BR")[0].value.city}}
* **Email**: {{in_lang_preferred(vcard.email, "pt-BR")[0].value}}
* **Notas adicionais**: {{in_lang_preferred(vcard.note, "pt-BR")[0].value}}

`vCard <{{vcard.source[0].value}}>`__

{% endfor %}
