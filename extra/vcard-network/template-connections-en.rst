{% for vcard in vcards %}
* `{{in_lang_preferred(vcard.fn, "en-US")[0].value}} <{{in_lang_preferred(vcard.url, "en-US")[0].value}}>`__: `vCard <{{in_lang_preferred(vcard.source, "en-US")[0].value}}>`__
{% endfor %}
